(function() {
  var getVimState;

  getVimState = require('./spec-helper').getVimState;

  describe("Insert mode commands", function() {
    var editor, editorElement, ensure, keystroke, ref, set, vimState;
    ref = [], set = ref[0], ensure = ref[1], keystroke = ref[2], editor = ref[3], editorElement = ref[4], vimState = ref[5];
    beforeEach(function() {
      return getVimState(function(_vimState, vim) {
        vimState = _vimState;
        editor = _vimState.editor, editorElement = _vimState.editorElement;
        return set = vim.set, ensure = vim.ensure, keystroke = vim.keystroke, vim;
      });
    });
    return describe("Copy from line above/below", function() {
      beforeEach(function() {
        set({
          text: "12345\n\nabcd\nefghi",
          cursor: [[1, 0], [3, 0]]
        });
        return keystroke('i');
      });
      describe("the ctrl-y command", function() {
        it("copies from the line above", function() {
          ensure('ctrl-y', {
            text: "12345\n1\nabcd\naefghi"
          });
          editor.insertText(' ');
          return ensure('ctrl-y', {
            text: "12345\n1 3\nabcd\na cefghi"
          });
        });
        it("does nothing if there's nothing above the cursor", function() {
          editor.insertText('fill');
          ensure('ctrl-y', {
            text: "12345\nfill5\nabcd\nfillefghi"
          });
          return ensure('ctrl-y', {
            text: "12345\nfill5\nabcd\nfillefghi"
          });
        });
        return it("does nothing on the first line", function() {
          set({
            cursor: [[0, 2], [3, 2]]
          });
          editor.insertText('a');
          ensure({
            text: "12a345\n\nabcd\nefaghi"
          });
          return ensure('ctrl-y', {
            text: "12a345\n\nabcd\nefadghi"
          });
        });
      });
      describe("the ctrl-e command", function() {
        beforeEach(function() {
          return atom.keymaps.add("test", {
            'atom-text-editor.vim-mode-plus.insert-mode': {
              'ctrl-e': 'vim-mode-plus:copy-from-line-below'
            }
          });
        });
        it("copies from the line below", function() {
          ensure('ctrl-e', {
            text: "12345\na\nabcd\nefghi"
          });
          editor.insertText(' ');
          return ensure('ctrl-e', {
            text: "12345\na c\nabcd\n efghi"
          });
        });
        return it("does nothing if there's nothing below the cursor", function() {
          editor.insertText('foo');
          ensure('ctrl-e', {
            text: "12345\nfood\nabcd\nfooefghi"
          });
          return ensure('ctrl-e', {
            text: "12345\nfood\nabcd\nfooefghi"
          });
        });
      });
      return describe("InsertLastInserted", function() {
        var ensureInsertLastInserted;
        ensureInsertLastInserted = function(key, options) {
          var finalText, insert, text;
          insert = options.insert, text = options.text, finalText = options.finalText;
          keystroke(key);
          editor.insertText(insert);
          ensure("escape", {
            text: text
          });
          return ensure("G I ctrl-a", {
            text: finalText
          });
        };
        beforeEach(function() {
          var initialText;
          atom.keymaps.add("test", {
            'atom-text-editor.vim-mode-plus.insert-mode': {
              'ctrl-a': 'vim-mode-plus:insert-last-inserted'
            }
          });
          initialText = "abc\ndef\n";
          set({
            text: "",
            cursor: [0, 0]
          });
          keystroke('i');
          editor.insertText(initialText);
          return ensure("escape g g", {
            text: initialText,
            cursor: [0, 0]
          });
        });
        it("case-i: single-line", function() {
          return ensureInsertLastInserted('i', {
            insert: 'xxx',
            text: "xxxabc\ndef\n",
            finalText: "xxxabc\nxxxdef\n"
          });
        });
        it("case-o: single-line", function() {
          return ensureInsertLastInserted('o', {
            insert: 'xxx',
            text: "abc\nxxx\ndef\n",
            finalText: "abc\nxxx\nxxxdef\n"
          });
        });
        it("case-O: single-line", function() {
          return ensureInsertLastInserted('O', {
            insert: 'xxx',
            text: "xxx\nabc\ndef\n",
            finalText: "xxx\nabc\nxxxdef\n"
          });
        });
        it("case-i: multi-line", function() {
          return ensureInsertLastInserted('i', {
            insert: 'xxx\nyyy\n',
            text: "xxx\nyyy\nabc\ndef\n",
            finalText: "xxx\nyyy\nabc\nxxx\nyyy\ndef\n"
          });
        });
        it("case-o: multi-line", function() {
          return ensureInsertLastInserted('o', {
            insert: 'xxx\nyyy\n',
            text: "abc\nxxx\nyyy\n\ndef\n",
            finalText: "abc\nxxx\nyyy\n\nxxx\nyyy\ndef\n"
          });
        });
        return it("case-O: multi-line", function() {
          return ensureInsertLastInserted('O', {
            insert: 'xxx\nyyy\n',
            text: "xxx\nyyy\n\nabc\ndef\n",
            finalText: "xxx\nyyy\n\nabc\nxxx\nyyy\ndef\n"
          });
        });
      });
    });
  });

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL1VzZXJzL293YWluLy5hdG9tL3BhY2thZ2VzL3ZpbS1tb2RlLXBsdXMvc3BlYy9pbnNlcnQtbW9kZS1zcGVjLmNvZmZlZSJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUFBLE1BQUE7O0VBQUMsY0FBZSxPQUFBLENBQVEsZUFBUjs7RUFFaEIsUUFBQSxDQUFTLHNCQUFULEVBQWlDLFNBQUE7QUFDL0IsUUFBQTtJQUFBLE1BQTRELEVBQTVELEVBQUMsWUFBRCxFQUFNLGVBQU4sRUFBYyxrQkFBZCxFQUF5QixlQUF6QixFQUFpQyxzQkFBakMsRUFBZ0Q7SUFFaEQsVUFBQSxDQUFXLFNBQUE7YUFDVCxXQUFBLENBQVksU0FBQyxTQUFELEVBQVksR0FBWjtRQUNWLFFBQUEsR0FBVztRQUNWLHlCQUFELEVBQVM7ZUFDUixhQUFELEVBQU0sbUJBQU4sRUFBYyx5QkFBZCxFQUEyQjtNQUhqQixDQUFaO0lBRFMsQ0FBWDtXQU1BLFFBQUEsQ0FBUyw0QkFBVCxFQUF1QyxTQUFBO01BQ3JDLFVBQUEsQ0FBVyxTQUFBO1FBQ1QsR0FBQSxDQUNFO1VBQUEsSUFBQSxFQUFNLHNCQUFOO1VBTUEsTUFBQSxFQUFRLENBQUMsQ0FBQyxDQUFELEVBQUksQ0FBSixDQUFELEVBQVMsQ0FBQyxDQUFELEVBQUksQ0FBSixDQUFULENBTlI7U0FERjtlQVFBLFNBQUEsQ0FBVSxHQUFWO01BVFMsQ0FBWDtNQVdBLFFBQUEsQ0FBUyxvQkFBVCxFQUErQixTQUFBO1FBQzdCLEVBQUEsQ0FBRyw0QkFBSCxFQUFpQyxTQUFBO1VBQy9CLE1BQUEsQ0FBTyxRQUFQLEVBQ0U7WUFBQSxJQUFBLEVBQU0sd0JBQU47V0FERjtVQU9BLE1BQU0sQ0FBQyxVQUFQLENBQWtCLEdBQWxCO2lCQUNBLE1BQUEsQ0FBTyxRQUFQLEVBQ0U7WUFBQSxJQUFBLEVBQU0sNEJBQU47V0FERjtRQVQrQixDQUFqQztRQWlCQSxFQUFBLENBQUcsa0RBQUgsRUFBdUQsU0FBQTtVQUNyRCxNQUFNLENBQUMsVUFBUCxDQUFrQixNQUFsQjtVQUNBLE1BQUEsQ0FBTyxRQUFQLEVBQ0U7WUFBQSxJQUFBLEVBQU0sK0JBQU47V0FERjtpQkFPQSxNQUFBLENBQU8sUUFBUCxFQUNFO1lBQUEsSUFBQSxFQUFNLCtCQUFOO1dBREY7UUFUcUQsQ0FBdkQ7ZUFpQkEsRUFBQSxDQUFHLGdDQUFILEVBQXFDLFNBQUE7VUFDbkMsR0FBQSxDQUNFO1lBQUEsTUFBQSxFQUFRLENBQUMsQ0FBQyxDQUFELEVBQUksQ0FBSixDQUFELEVBQVMsQ0FBQyxDQUFELEVBQUksQ0FBSixDQUFULENBQVI7V0FERjtVQUVBLE1BQU0sQ0FBQyxVQUFQLENBQWtCLEdBQWxCO1VBQ0EsTUFBQSxDQUNFO1lBQUEsSUFBQSxFQUFNLHdCQUFOO1dBREY7aUJBT0EsTUFBQSxDQUFPLFFBQVAsRUFDRTtZQUFBLElBQUEsRUFBTSx5QkFBTjtXQURGO1FBWG1DLENBQXJDO01BbkM2QixDQUEvQjtNQXNEQSxRQUFBLENBQVMsb0JBQVQsRUFBK0IsU0FBQTtRQUM3QixVQUFBLENBQVcsU0FBQTtpQkFDVCxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQWIsQ0FBaUIsTUFBakIsRUFDRTtZQUFBLDRDQUFBLEVBQ0U7Y0FBQSxRQUFBLEVBQVUsb0NBQVY7YUFERjtXQURGO1FBRFMsQ0FBWDtRQUtBLEVBQUEsQ0FBRyw0QkFBSCxFQUFpQyxTQUFBO1VBQy9CLE1BQUEsQ0FBTyxRQUFQLEVBQ0U7WUFBQSxJQUFBLEVBQU0sdUJBQU47V0FERjtVQU9BLE1BQU0sQ0FBQyxVQUFQLENBQWtCLEdBQWxCO2lCQUNBLE1BQUEsQ0FBTyxRQUFQLEVBQ0U7WUFBQSxJQUFBLEVBQU0sMEJBQU47V0FERjtRQVQrQixDQUFqQztlQWlCQSxFQUFBLENBQUcsa0RBQUgsRUFBdUQsU0FBQTtVQUNyRCxNQUFNLENBQUMsVUFBUCxDQUFrQixLQUFsQjtVQUNBLE1BQUEsQ0FBTyxRQUFQLEVBQ0U7WUFBQSxJQUFBLEVBQU0sNkJBQU47V0FERjtpQkFPQSxNQUFBLENBQU8sUUFBUCxFQUNFO1lBQUEsSUFBQSxFQUFNLDZCQUFOO1dBREY7UUFUcUQsQ0FBdkQ7TUF2QjZCLENBQS9CO2FBd0NBLFFBQUEsQ0FBUyxvQkFBVCxFQUErQixTQUFBO0FBQzdCLFlBQUE7UUFBQSx3QkFBQSxHQUEyQixTQUFDLEdBQUQsRUFBTSxPQUFOO0FBQ3pCLGNBQUE7VUFBQyx1QkFBRCxFQUFTLG1CQUFULEVBQWU7VUFDZixTQUFBLENBQVUsR0FBVjtVQUNBLE1BQU0sQ0FBQyxVQUFQLENBQWtCLE1BQWxCO1VBQ0EsTUFBQSxDQUFPLFFBQVAsRUFBaUI7WUFBQSxJQUFBLEVBQU0sSUFBTjtXQUFqQjtpQkFDQSxNQUFBLENBQU8sWUFBUCxFQUFxQjtZQUFBLElBQUEsRUFBTSxTQUFOO1dBQXJCO1FBTHlCO1FBTzNCLFVBQUEsQ0FBVyxTQUFBO0FBQ1QsY0FBQTtVQUFBLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBYixDQUFpQixNQUFqQixFQUNFO1lBQUEsNENBQUEsRUFDRTtjQUFBLFFBQUEsRUFBVSxvQ0FBVjthQURGO1dBREY7VUFJQSxXQUFBLEdBQWM7VUFJZCxHQUFBLENBQUk7WUFBQSxJQUFBLEVBQU0sRUFBTjtZQUFVLE1BQUEsRUFBUSxDQUFDLENBQUQsRUFBSSxDQUFKLENBQWxCO1dBQUo7VUFDQSxTQUFBLENBQVUsR0FBVjtVQUNBLE1BQU0sQ0FBQyxVQUFQLENBQWtCLFdBQWxCO2lCQUNBLE1BQUEsQ0FBTyxZQUFQLEVBQ0U7WUFBQSxJQUFBLEVBQU0sV0FBTjtZQUNBLE1BQUEsRUFBUSxDQUFDLENBQUQsRUFBSSxDQUFKLENBRFI7V0FERjtRQVpTLENBQVg7UUFnQkEsRUFBQSxDQUFHLHFCQUFILEVBQTBCLFNBQUE7aUJBQ3hCLHdCQUFBLENBQXlCLEdBQXpCLEVBQ0U7WUFBQSxNQUFBLEVBQVEsS0FBUjtZQUNBLElBQUEsRUFBTSxlQUROO1lBRUEsU0FBQSxFQUFXLGtCQUZYO1dBREY7UUFEd0IsQ0FBMUI7UUFLQSxFQUFBLENBQUcscUJBQUgsRUFBMEIsU0FBQTtpQkFDeEIsd0JBQUEsQ0FBeUIsR0FBekIsRUFDRTtZQUFBLE1BQUEsRUFBUSxLQUFSO1lBQ0EsSUFBQSxFQUFNLGlCQUROO1lBRUEsU0FBQSxFQUFXLG9CQUZYO1dBREY7UUFEd0IsQ0FBMUI7UUFLQSxFQUFBLENBQUcscUJBQUgsRUFBMEIsU0FBQTtpQkFDeEIsd0JBQUEsQ0FBeUIsR0FBekIsRUFDRTtZQUFBLE1BQUEsRUFBUSxLQUFSO1lBQ0EsSUFBQSxFQUFNLGlCQUROO1lBRUEsU0FBQSxFQUFXLG9CQUZYO1dBREY7UUFEd0IsQ0FBMUI7UUFNQSxFQUFBLENBQUcsb0JBQUgsRUFBeUIsU0FBQTtpQkFDdkIsd0JBQUEsQ0FBeUIsR0FBekIsRUFDRTtZQUFBLE1BQUEsRUFBUSxZQUFSO1lBQ0EsSUFBQSxFQUFNLHNCQUROO1lBRUEsU0FBQSxFQUFXLGdDQUZYO1dBREY7UUFEdUIsQ0FBekI7UUFLQSxFQUFBLENBQUcsb0JBQUgsRUFBeUIsU0FBQTtpQkFDdkIsd0JBQUEsQ0FBeUIsR0FBekIsRUFDRTtZQUFBLE1BQUEsRUFBUSxZQUFSO1lBQ0EsSUFBQSxFQUFNLHdCQUROO1lBRUEsU0FBQSxFQUFXLGtDQUZYO1dBREY7UUFEdUIsQ0FBekI7ZUFLQSxFQUFBLENBQUcsb0JBQUgsRUFBeUIsU0FBQTtpQkFDdkIsd0JBQUEsQ0FBeUIsR0FBekIsRUFDRTtZQUFBLE1BQUEsRUFBUSxZQUFSO1lBQ0EsSUFBQSxFQUFNLHdCQUROO1lBRUEsU0FBQSxFQUFXLGtDQUZYO1dBREY7UUFEdUIsQ0FBekI7TUFsRDZCLENBQS9CO0lBMUdxQyxDQUF2QztFQVQrQixDQUFqQztBQUZBIiwic291cmNlc0NvbnRlbnQiOlsie2dldFZpbVN0YXRlfSA9IHJlcXVpcmUgJy4vc3BlYy1oZWxwZXInXG5cbmRlc2NyaWJlIFwiSW5zZXJ0IG1vZGUgY29tbWFuZHNcIiwgLT5cbiAgW3NldCwgZW5zdXJlLCBrZXlzdHJva2UsIGVkaXRvciwgZWRpdG9yRWxlbWVudCwgdmltU3RhdGVdID0gW11cblxuICBiZWZvcmVFYWNoIC0+XG4gICAgZ2V0VmltU3RhdGUgKF92aW1TdGF0ZSwgdmltKSAtPlxuICAgICAgdmltU3RhdGUgPSBfdmltU3RhdGVcbiAgICAgIHtlZGl0b3IsIGVkaXRvckVsZW1lbnR9ID0gX3ZpbVN0YXRlXG4gICAgICB7c2V0LCBlbnN1cmUsIGtleXN0cm9rZX0gPSB2aW1cblxuICBkZXNjcmliZSBcIkNvcHkgZnJvbSBsaW5lIGFib3ZlL2JlbG93XCIsIC0+XG4gICAgYmVmb3JlRWFjaCAtPlxuICAgICAgc2V0XG4gICAgICAgIHRleHQ6IFwiXCJcIlxuICAgICAgICAgIDEyMzQ1XG5cbiAgICAgICAgICBhYmNkXG4gICAgICAgICAgZWZnaGlcbiAgICAgICAgICBcIlwiXCJcbiAgICAgICAgY3Vyc29yOiBbWzEsIDBdLCBbMywgMF1dXG4gICAgICBrZXlzdHJva2UgJ2knXG5cbiAgICBkZXNjcmliZSBcInRoZSBjdHJsLXkgY29tbWFuZFwiLCAtPlxuICAgICAgaXQgXCJjb3BpZXMgZnJvbSB0aGUgbGluZSBhYm92ZVwiLCAtPlxuICAgICAgICBlbnN1cmUgJ2N0cmwteScsXG4gICAgICAgICAgdGV4dDogXCJcIlwiXG4gICAgICAgICAgICAxMjM0NVxuICAgICAgICAgICAgMVxuICAgICAgICAgICAgYWJjZFxuICAgICAgICAgICAgYWVmZ2hpXG4gICAgICAgICAgICBcIlwiXCJcbiAgICAgICAgZWRpdG9yLmluc2VydFRleHQgJyAnXG4gICAgICAgIGVuc3VyZSAnY3RybC15JyxcbiAgICAgICAgICB0ZXh0OiBcIlwiXCJcbiAgICAgICAgICAgIDEyMzQ1XG4gICAgICAgICAgICAxIDNcbiAgICAgICAgICAgIGFiY2RcbiAgICAgICAgICAgIGEgY2VmZ2hpXG4gICAgICAgICAgICBcIlwiXCJcblxuICAgICAgaXQgXCJkb2VzIG5vdGhpbmcgaWYgdGhlcmUncyBub3RoaW5nIGFib3ZlIHRoZSBjdXJzb3JcIiwgLT5cbiAgICAgICAgZWRpdG9yLmluc2VydFRleHQgJ2ZpbGwnXG4gICAgICAgIGVuc3VyZSAnY3RybC15JyxcbiAgICAgICAgICB0ZXh0OiBcIlwiXCJcbiAgICAgICAgICAgIDEyMzQ1XG4gICAgICAgICAgICBmaWxsNVxuICAgICAgICAgICAgYWJjZFxuICAgICAgICAgICAgZmlsbGVmZ2hpXG4gICAgICAgICAgICBcIlwiXCJcbiAgICAgICAgZW5zdXJlICdjdHJsLXknLFxuICAgICAgICAgIHRleHQ6IFwiXCJcIlxuICAgICAgICAgICAgMTIzNDVcbiAgICAgICAgICAgIGZpbGw1XG4gICAgICAgICAgICBhYmNkXG4gICAgICAgICAgICBmaWxsZWZnaGlcbiAgICAgICAgICAgIFwiXCJcIlxuXG4gICAgICBpdCBcImRvZXMgbm90aGluZyBvbiB0aGUgZmlyc3QgbGluZVwiLCAtPlxuICAgICAgICBzZXRcbiAgICAgICAgICBjdXJzb3I6IFtbMCwgMl0sIFszLCAyXV1cbiAgICAgICAgZWRpdG9yLmluc2VydFRleHQgJ2EnXG4gICAgICAgIGVuc3VyZVxuICAgICAgICAgIHRleHQ6IFwiXCJcIlxuICAgICAgICAgICAgMTJhMzQ1XG5cbiAgICAgICAgICAgIGFiY2RcbiAgICAgICAgICAgIGVmYWdoaVxuICAgICAgICAgICAgXCJcIlwiXG4gICAgICAgIGVuc3VyZSAnY3RybC15JyxcbiAgICAgICAgICB0ZXh0OiBcIlwiXCJcbiAgICAgICAgICAgIDEyYTM0NVxuXG4gICAgICAgICAgICBhYmNkXG4gICAgICAgICAgICBlZmFkZ2hpXG4gICAgICAgICAgICBcIlwiXCJcblxuICAgIGRlc2NyaWJlIFwidGhlIGN0cmwtZSBjb21tYW5kXCIsIC0+XG4gICAgICBiZWZvcmVFYWNoIC0+XG4gICAgICAgIGF0b20ua2V5bWFwcy5hZGQgXCJ0ZXN0XCIsXG4gICAgICAgICAgJ2F0b20tdGV4dC1lZGl0b3IudmltLW1vZGUtcGx1cy5pbnNlcnQtbW9kZSc6XG4gICAgICAgICAgICAnY3RybC1lJzogJ3ZpbS1tb2RlLXBsdXM6Y29weS1mcm9tLWxpbmUtYmVsb3cnXG5cbiAgICAgIGl0IFwiY29waWVzIGZyb20gdGhlIGxpbmUgYmVsb3dcIiwgLT5cbiAgICAgICAgZW5zdXJlICdjdHJsLWUnLFxuICAgICAgICAgIHRleHQ6IFwiXCJcIlxuICAgICAgICAgICAgMTIzNDVcbiAgICAgICAgICAgIGFcbiAgICAgICAgICAgIGFiY2RcbiAgICAgICAgICAgIGVmZ2hpXG4gICAgICAgICAgICBcIlwiXCJcbiAgICAgICAgZWRpdG9yLmluc2VydFRleHQgJyAnXG4gICAgICAgIGVuc3VyZSAnY3RybC1lJyxcbiAgICAgICAgICB0ZXh0OiBcIlwiXCJcbiAgICAgICAgICAgIDEyMzQ1XG4gICAgICAgICAgICBhIGNcbiAgICAgICAgICAgIGFiY2RcbiAgICAgICAgICAgICBlZmdoaVxuICAgICAgICAgICAgXCJcIlwiXG5cbiAgICAgIGl0IFwiZG9lcyBub3RoaW5nIGlmIHRoZXJlJ3Mgbm90aGluZyBiZWxvdyB0aGUgY3Vyc29yXCIsIC0+XG4gICAgICAgIGVkaXRvci5pbnNlcnRUZXh0ICdmb28nXG4gICAgICAgIGVuc3VyZSAnY3RybC1lJyxcbiAgICAgICAgICB0ZXh0OiBcIlwiXCJcbiAgICAgICAgICAgIDEyMzQ1XG4gICAgICAgICAgICBmb29kXG4gICAgICAgICAgICBhYmNkXG4gICAgICAgICAgICBmb29lZmdoaVxuICAgICAgICAgICAgXCJcIlwiXG4gICAgICAgIGVuc3VyZSAnY3RybC1lJyxcbiAgICAgICAgICB0ZXh0OiBcIlwiXCJcbiAgICAgICAgICAgIDEyMzQ1XG4gICAgICAgICAgICBmb29kXG4gICAgICAgICAgICBhYmNkXG4gICAgICAgICAgICBmb29lZmdoaVxuICAgICAgICAgICAgXCJcIlwiXG5cbiAgICBkZXNjcmliZSBcIkluc2VydExhc3RJbnNlcnRlZFwiLCAtPlxuICAgICAgZW5zdXJlSW5zZXJ0TGFzdEluc2VydGVkID0gKGtleSwgb3B0aW9ucykgLT5cbiAgICAgICAge2luc2VydCwgdGV4dCwgZmluYWxUZXh0fSA9IG9wdGlvbnNcbiAgICAgICAga2V5c3Ryb2tlIGtleVxuICAgICAgICBlZGl0b3IuaW5zZXJ0VGV4dChpbnNlcnQpXG4gICAgICAgIGVuc3VyZSBcImVzY2FwZVwiLCB0ZXh0OiB0ZXh0XG4gICAgICAgIGVuc3VyZSBcIkcgSSBjdHJsLWFcIiwgdGV4dDogZmluYWxUZXh0XG5cbiAgICAgIGJlZm9yZUVhY2ggLT5cbiAgICAgICAgYXRvbS5rZXltYXBzLmFkZCBcInRlc3RcIixcbiAgICAgICAgICAnYXRvbS10ZXh0LWVkaXRvci52aW0tbW9kZS1wbHVzLmluc2VydC1tb2RlJzpcbiAgICAgICAgICAgICdjdHJsLWEnOiAndmltLW1vZGUtcGx1czppbnNlcnQtbGFzdC1pbnNlcnRlZCdcblxuICAgICAgICBpbml0aWFsVGV4dCA9IFwiXCJcIlxuICAgICAgICAgIGFiY1xuICAgICAgICAgIGRlZlxcblxuICAgICAgICAgIFwiXCJcIlxuICAgICAgICBzZXQgdGV4dDogXCJcIiwgY3Vyc29yOiBbMCwgMF1cbiAgICAgICAga2V5c3Ryb2tlICdpJ1xuICAgICAgICBlZGl0b3IuaW5zZXJ0VGV4dChpbml0aWFsVGV4dClcbiAgICAgICAgZW5zdXJlIFwiZXNjYXBlIGcgZ1wiLFxuICAgICAgICAgIHRleHQ6IGluaXRpYWxUZXh0XG4gICAgICAgICAgY3Vyc29yOiBbMCwgMF1cblxuICAgICAgaXQgXCJjYXNlLWk6IHNpbmdsZS1saW5lXCIsIC0+XG4gICAgICAgIGVuc3VyZUluc2VydExhc3RJbnNlcnRlZCAnaScsXG4gICAgICAgICAgaW5zZXJ0OiAneHh4J1xuICAgICAgICAgIHRleHQ6IFwieHh4YWJjXFxuZGVmXFxuXCJcbiAgICAgICAgICBmaW5hbFRleHQ6IFwieHh4YWJjXFxueHh4ZGVmXFxuXCJcbiAgICAgIGl0IFwiY2FzZS1vOiBzaW5nbGUtbGluZVwiLCAtPlxuICAgICAgICBlbnN1cmVJbnNlcnRMYXN0SW5zZXJ0ZWQgJ28nLFxuICAgICAgICAgIGluc2VydDogJ3h4eCdcbiAgICAgICAgICB0ZXh0OiBcImFiY1xcbnh4eFxcbmRlZlxcblwiXG4gICAgICAgICAgZmluYWxUZXh0OiBcImFiY1xcbnh4eFxcbnh4eGRlZlxcblwiXG4gICAgICBpdCBcImNhc2UtTzogc2luZ2xlLWxpbmVcIiwgLT5cbiAgICAgICAgZW5zdXJlSW5zZXJ0TGFzdEluc2VydGVkICdPJyxcbiAgICAgICAgICBpbnNlcnQ6ICd4eHgnXG4gICAgICAgICAgdGV4dDogXCJ4eHhcXG5hYmNcXG5kZWZcXG5cIlxuICAgICAgICAgIGZpbmFsVGV4dDogXCJ4eHhcXG5hYmNcXG54eHhkZWZcXG5cIlxuXG4gICAgICBpdCBcImNhc2UtaTogbXVsdGktbGluZVwiLCAtPlxuICAgICAgICBlbnN1cmVJbnNlcnRMYXN0SW5zZXJ0ZWQgJ2knLFxuICAgICAgICAgIGluc2VydDogJ3h4eFxcbnl5eVxcbidcbiAgICAgICAgICB0ZXh0OiBcInh4eFxcbnl5eVxcbmFiY1xcbmRlZlxcblwiXG4gICAgICAgICAgZmluYWxUZXh0OiBcInh4eFxcbnl5eVxcbmFiY1xcbnh4eFxcbnl5eVxcbmRlZlxcblwiXG4gICAgICBpdCBcImNhc2UtbzogbXVsdGktbGluZVwiLCAtPlxuICAgICAgICBlbnN1cmVJbnNlcnRMYXN0SW5zZXJ0ZWQgJ28nLFxuICAgICAgICAgIGluc2VydDogJ3h4eFxcbnl5eVxcbidcbiAgICAgICAgICB0ZXh0OiBcImFiY1xcbnh4eFxcbnl5eVxcblxcbmRlZlxcblwiXG4gICAgICAgICAgZmluYWxUZXh0OiBcImFiY1xcbnh4eFxcbnl5eVxcblxcbnh4eFxcbnl5eVxcbmRlZlxcblwiXG4gICAgICBpdCBcImNhc2UtTzogbXVsdGktbGluZVwiLCAtPlxuICAgICAgICBlbnN1cmVJbnNlcnRMYXN0SW5zZXJ0ZWQgJ08nLFxuICAgICAgICAgIGluc2VydDogJ3h4eFxcbnl5eVxcbidcbiAgICAgICAgICB0ZXh0OiBcInh4eFxcbnl5eVxcblxcbmFiY1xcbmRlZlxcblwiXG4gICAgICAgICAgZmluYWxUZXh0OiBcInh4eFxcbnl5eVxcblxcbmFiY1xcbnh4eFxcbnl5eVxcbmRlZlxcblwiXG4iXX0=
