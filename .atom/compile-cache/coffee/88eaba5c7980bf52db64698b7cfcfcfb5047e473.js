(function() {
  var AutoIndent, Point, Range, fs, path, ref;

  ref = require('atom'), Range = ref.Range, Point = ref.Point;

  fs = require('fs-plus');

  path = require('path');

  AutoIndent = require('../lib/auto-indent');

  describe('auto-indent', function() {
    var autoIndent, editor, notifications, ref1, sourceCode, sourceCodeRange;
    ref1 = [], autoIndent = ref1[0], editor = ref1[1], notifications = ref1[2], sourceCode = ref1[3], sourceCodeRange = ref1[4];
    beforeEach(function() {
      return waitsForPromise(function() {
        return atom.packages.activatePackage('language-babel');
      });
    });
    beforeEach(function() {
      waitsForPromise(function() {
        return atom.workspace.open('non-existent.js').then(function(o) {
          return editor = o;
        });
      });
      return runs(function() {
        autoIndent = new AutoIndent(editor);
        return notifications = atom.notifications;
      });
    });
    describe('::constructor', function() {
      return it(' should setup some valid indentation defaults', function() {
        var expectedResult;
        expectedResult = {
          jsxIndent: [1, 1],
          jsxIndentProps: [1, 1],
          jsxClosingBracketLocation: [
            1, {
              selfClosing: 'tag-aligned',
              nonEmpty: 'tag-aligned'
            }
          ]
        };
        return expect(autoIndent.eslintIndentOptions).toEqual(expectedResult);
      });
    });
    describe('::getEslintrcFilename', function() {
      it('returns a correct project path for the source file', function() {
        return expect(path.dirname(autoIndent.getEslintrcFilename())).toEqual(path.dirname(editor.getPath()));
      });
      return it('returns a .eslintrc file name', function() {
        return expect(path.basename(autoIndent.getEslintrcFilename())).toEqual('.eslintrc');
      });
    });
    return describe('::readEslintrcOptions', function() {
      it('returns an empty object on a missing .eslintrc', function() {
        return expect(autoIndent.readEslintrcOptions('.missing')).toEqual({});
      });
      it('returns and empty Object and a notification message on bad eslint', function() {
        var obj;
        spyOn(fs, 'existsSync').andReturn(true);
        spyOn(fs, 'readFileSync').andReturn('{');
        spyOn(notifications, 'addError').andCallThrough();
        obj = autoIndent.readEslintrcOptions();
        expect(notifications.addError).toHaveBeenCalled();
        return expect(obj).toEqual({});
      });
      it('returns an empty Object when eslint with no rules is read', function() {
        var obj;
        spyOn(fs, 'existsSync').andReturn(true);
        spyOn(fs, 'readFileSync').andReturn('{}');
        spyOn(notifications, 'addError').andCallThrough();
        obj = autoIndent.readEslintrcOptions();
        expect(notifications.addError).not.toHaveBeenCalled();
        return expect(obj).toEqual({});
      });
      describe('::translateIndentOptions', function() {
        it('should return expected defaults when no object is input', function() {
          var expectedResult, result;
          result = autoIndent.translateIndentOptions();
          expectedResult = {
            jsxIndent: [1, 1],
            jsxIndentProps: [1, 1],
            jsxClosingBracketLocation: [
              1, {
                selfClosing: 'tag-aligned',
                nonEmpty: 'tag-aligned'
              }
            ]
          };
          return expect(result).toEqual(expectedResult);
        });
        it('should return expected defaults when no valid object is input', function() {
          var expectedResult, result;
          result = autoIndent.translateIndentOptions({});
          expectedResult = {
            jsxIndent: [1, 1],
            jsxIndentProps: [1, 1],
            jsxClosingBracketLocation: [
              1, {
                selfClosing: 'tag-aligned',
                nonEmpty: 'tag-aligned'
              }
            ]
          };
          return expect(result).toEqual(expectedResult);
        });
        it('should return two tab markers for jsx and props when an indent of 4 spaces is found', function() {
          var expectedResult, result, rules;
          rules = {
            "indent": [1, 4]
          };
          result = autoIndent.translateIndentOptions(rules);
          expectedResult = {
            jsxIndent: [1, 2],
            jsxIndentProps: [1, 2],
            jsxClosingBracketLocation: [
              1, {
                selfClosing: 'tag-aligned',
                nonEmpty: 'tag-aligned'
              }
            ]
          };
          return expect(result).toEqual(expectedResult);
        });
        it('should return one tab markers for jsx and props when an indent "tab" is found', function() {
          var expectedResult, result, rules;
          rules = {
            "indent": [1, "tab"]
          };
          result = autoIndent.translateIndentOptions(rules);
          expectedResult = {
            jsxIndent: [1, 1],
            jsxIndentProps: [1, 1],
            jsxClosingBracketLocation: [
              1, {
                selfClosing: 'tag-aligned',
                nonEmpty: 'tag-aligned'
              }
            ]
          };
          return expect(result).toEqual(expectedResult);
        });
        it('should return jsxIndent of 2 tabs and jsxIndentProps of 3', function() {
          var expectedResult, result, rules;
          rules = {
            "indent": [1, 6],
            "react/jsx-indent": ["warn", 4]
          };
          result = autoIndent.translateIndentOptions(rules);
          expectedResult = {
            jsxIndent: ['warn', 2],
            jsxIndentProps: [1, 3],
            jsxClosingBracketLocation: [
              1, {
                selfClosing: 'tag-aligned',
                nonEmpty: 'tag-aligned'
              }
            ]
          };
          return expect(result).toEqual(expectedResult);
        });
        it('should return jsxIndent of 2 tabs and jsxIndentProps of 2', function() {
          var expectedResult, result, rules;
          rules = {
            "indent": [1, 6],
            "react/jsx-indent": ["warn", 4],
            "react/jsx-indent-props": [2, 4]
          };
          result = autoIndent.translateIndentOptions(rules);
          expectedResult = {
            jsxIndent: ['warn', 2],
            jsxIndentProps: [2, 2],
            jsxClosingBracketLocation: [
              1, {
                selfClosing: 'tag-aligned',
                nonEmpty: 'tag-aligned'
              }
            ]
          };
          return expect(result).toEqual(expectedResult);
        });
        it('should return jsxIndent of 2 tabs and jsxIndentProps of 2, line-aligned', function() {
          var expectedResult, result, rules;
          rules = {
            "indent": [1, 6],
            "react/jsx-indent": ["warn", 4],
            "react/jsx-indent-props": [2, 4],
            'react/jsx-closing-bracket-location': [1, 'line-aligned']
          };
          result = autoIndent.translateIndentOptions(rules);
          expectedResult = {
            jsxIndent: ['warn', 2],
            jsxIndentProps: [2, 2],
            jsxClosingBracketLocation: [
              1, {
                selfClosing: 'line-aligned',
                nonEmpty: 'line-aligned'
              }
            ]
          };
          return expect(result).toEqual(expectedResult);
        });
        return it('should return jsxIndent of 2 tabs and jsxIndentProps of 2, line-aligned and props-aligned', function() {
          var expectedResult, result, rules;
          rules = {
            "indent": [1, 6],
            "react/jsx-indent": ["warn", 4],
            "react/jsx-indent-props": [2, 4],
            "react/jsx-closing-bracket-location": [
              1, {
                "nonEmpty": "props-aligned",
                "selfClosing": "line-aligned"
              }
            ]
          };
          result = autoIndent.translateIndentOptions(rules);
          expectedResult = {
            jsxIndent: ['warn', 2],
            jsxIndentProps: [2, 2],
            jsxClosingBracketLocation: [
              1, {
                selfClosing: 'line-aligned',
                nonEmpty: 'props-aligned'
              }
            ]
          };
          return expect(result).toEqual(expectedResult);
        });
      });
      describe('::indentJSX', function() {
        beforeEach(function() {
          sourceCode = "<div className={rootClass}>\n{this._renderPlaceholder()}\n<div\nclassName={cx('DraftEditor/editorContainer')}\nkey={'editor' + this.state.containerKey}\nref=\"editorContainer\"\n>\n<div\naria-activedescendant={\nreadOnly ? null : this.props.ariaActiveDescendantID\n}\naria-autocomplete={readOnly ? null : this.props.ariaAutoComplete}\n>\n{this._renderPlaceholder()}\n<Component p1\np2\n/>\n</div>\n{ // tests inline JSX\nif (a) {\nreturn (\n<div></div>\n)\n}\nelse (b) {\nswitch (a) {\ncase 1:\nreturn (\n<div></div>\n)\ndefault:\n}\n}\n}\n</div>\n</div>";
          editor.insertText(sourceCode);
          return sourceCodeRange = new Range(new Point(0, 0), new Point(35, 6));
        });
        it('should indent JSX according to eslint rules', function() {
          var indentedCode;
          indentedCode = "<div className={rootClass}>\n    {this._renderPlaceholder()}\n    <div\n        className={cx('DraftEditor/editorContainer')}\n        key={'editor' + this.state.containerKey}\n        ref=\"editorContainer\"\n    >\n        <div\n            aria-activedescendant={\n                readOnly ? null : this.props.ariaActiveDescendantID\n            }\n            aria-autocomplete={readOnly ? null : this.props.ariaAutoComplete}\n        >\n            {this._renderPlaceholder()}\n            <Component p1\n                p2\n            />\n        </div>\n        { // tests inline JSX\n            if (a) {\n                return (\n                    <div></div>\n                )\n            }\n            else (b) {\n                switch (a) {\n                    case 1:\n                        return (\n                            <div></div>\n                        )\n                    default:\n                }\n            }\n        }\n    </div>\n</div>";
          autoIndent.eslintIndentOptions = {
            jsxIndent: [1, 2],
            jsxIndentProps: [1, 2],
            jsxClosingBracketLocation: [
              1, {
                selfClosing: 'tag-aligned',
                nonEmpty: 'tag-aligned'
              }
            ]
          };
          autoIndent.autoJsx = true;
          autoIndent.indentJSX(sourceCodeRange);
          return expect(editor.getTextInBufferRange(sourceCodeRange)).toEqual(indentedCode);
        });
        return it('should indent JSX according to eslint rules and tag closing alignment', function() {
          var indentedCode;
          indentedCode = "<div className={rootClass}>\n    {this._renderPlaceholder()}\n    <div\n        className={cx('DraftEditor/editorContainer')}\n        key={'editor' + this.state.containerKey}\n        ref=\"editorContainer\"\n        >\n        <div\n            aria-activedescendant={\n                readOnly ? null : this.props.ariaActiveDescendantID\n            }\n            aria-autocomplete={readOnly ? null : this.props.ariaAutoComplete}\n            >\n            {this._renderPlaceholder()}\n            <Component p1\n                p2\n                />\n        </div>\n        { // tests inline JSX\n            if (a) {\n                return (\n                    <div></div>\n                )\n            }\n            else (b) {\n                switch (a) {\n                    case 1:\n                        return (\n                            <div></div>\n                        )\n                    default:\n                }\n            }\n        }\n    </div>\n</div>";
          autoIndent.eslintIndentOptions = {
            jsxIndent: [1, 2],
            jsxIndentProps: [1, 2],
            jsxClosingBracketLocation: [
              1, {
                selfClosing: 'props-aligned',
                nonEmpty: 'props-aligned'
              }
            ]
          };
          autoIndent.autoJsx = true;
          autoIndent.indentJSX(sourceCodeRange);
          return expect(editor.getTextInBufferRange(sourceCodeRange)).toEqual(indentedCode);
        });
      });
      return describe('insert-nl-jsx', function() {
        return it('should insert two new lines and position cursor between JSX tags', function() {
          autoIndent.eslintIndentOptions = {
            jsxIndent: [1, 1],
            jsxIndentProps: [1, 1],
            jsxClosingBracketLocation: [
              1, {
                selfClosing: 'tabs-aligned',
                nonEmpty: 'tabs-aligned'
              }
            ]
          };
          autoIndent.autoJsx = true;
          editor.insertText('<div></div>');
          editor.setCursorBufferPosition([0, 5]);
          editor.insertText('\n');
          expect(editor.getTextInBufferRange([[0, 0], [0, 5]])).toEqual("<div>");
          expect(editor.getTextInBufferRange([[1, 0], [1, 2]])).toEqual("  ");
          expect(editor.getTextInBufferRange([[2, 0], [2, 6]])).toEqual("</div>");
          return expect(editor.getCursorBufferPosition()).toEqual([1, 2]);
        });
      });
    });
  });

}).call(this);

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiL1VzZXJzL293YWluLy5hdG9tL3BhY2thZ2VzL2xhbmd1YWdlLWJhYmVsL3NwZWMvYXV0by1pbmRlbnQtc3BlYy5jb2ZmZWUiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7QUFBQSxNQUFBOztFQUFBLE1BQWlCLE9BQUEsQ0FBUSxNQUFSLENBQWpCLEVBQUMsaUJBQUQsRUFBUTs7RUFDUixFQUFBLEdBQUssT0FBQSxDQUFRLFNBQVI7O0VBQ0wsSUFBQSxHQUFPLE9BQUEsQ0FBUSxNQUFSOztFQUNQLFVBQUEsR0FBYSxPQUFBLENBQVEsb0JBQVI7O0VBRWIsUUFBQSxDQUFTLGFBQVQsRUFBd0IsU0FBQTtBQUN0QixRQUFBO0lBQUEsT0FBbUUsRUFBbkUsRUFBQyxvQkFBRCxFQUFhLGdCQUFiLEVBQXFCLHVCQUFyQixFQUFvQyxvQkFBcEMsRUFBZ0Q7SUFFaEQsVUFBQSxDQUFXLFNBQUE7YUFDVCxlQUFBLENBQWdCLFNBQUE7ZUFDZCxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWQsQ0FBOEIsZ0JBQTlCO01BRGMsQ0FBaEI7SUFEUyxDQUFYO0lBSUEsVUFBQSxDQUFXLFNBQUE7TUFDVCxlQUFBLENBQWdCLFNBQUE7ZUFDZCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQWYsQ0FBb0IsaUJBQXBCLENBQXNDLENBQUMsSUFBdkMsQ0FBNEMsU0FBQyxDQUFEO2lCQUFPLE1BQUEsR0FBUztRQUFoQixDQUE1QztNQURjLENBQWhCO2FBR0EsSUFBQSxDQUFLLFNBQUE7UUFDSCxVQUFBLEdBQWlCLElBQUEsVUFBQSxDQUFXLE1BQVg7ZUFDakIsYUFBQSxHQUFnQixJQUFJLENBQUM7TUFGbEIsQ0FBTDtJQUpTLENBQVg7SUFVQSxRQUFBLENBQVMsZUFBVCxFQUEwQixTQUFBO2FBQ3hCLEVBQUEsQ0FBRywrQ0FBSCxFQUFvRCxTQUFBO0FBQ2xELFlBQUE7UUFBQSxjQUFBLEdBQ0U7VUFBQSxTQUFBLEVBQVcsQ0FBQyxDQUFELEVBQUcsQ0FBSCxDQUFYO1VBQ0EsY0FBQSxFQUFnQixDQUFDLENBQUQsRUFBRyxDQUFILENBRGhCO1VBRUEseUJBQUEsRUFBMkI7WUFBRSxDQUFGLEVBQUs7Y0FBRSxXQUFBLEVBQWEsYUFBZjtjQUE4QixRQUFBLEVBQVUsYUFBeEM7YUFBTDtXQUYzQjs7ZUFHRixNQUFBLENBQU8sVUFBVSxDQUFDLG1CQUFsQixDQUFzQyxDQUFDLE9BQXZDLENBQStDLGNBQS9DO01BTGtELENBQXBEO0lBRHdCLENBQTFCO0lBU0EsUUFBQSxDQUFTLHVCQUFULEVBQWtDLFNBQUE7TUFDaEMsRUFBQSxDQUFHLG9EQUFILEVBQXlELFNBQUE7ZUFDdkQsTUFBQSxDQUFPLElBQUksQ0FBQyxPQUFMLENBQWEsVUFBVSxDQUFDLG1CQUFYLENBQUEsQ0FBYixDQUFQLENBQXNELENBQUMsT0FBdkQsQ0FBK0QsSUFBSSxDQUFDLE9BQUwsQ0FBYSxNQUFNLENBQUMsT0FBUCxDQUFBLENBQWIsQ0FBL0Q7TUFEdUQsQ0FBekQ7YUFHQSxFQUFBLENBQUcsK0JBQUgsRUFBb0MsU0FBQTtlQUNsQyxNQUFBLENBQU8sSUFBSSxDQUFDLFFBQUwsQ0FBYyxVQUFVLENBQUMsbUJBQVgsQ0FBQSxDQUFkLENBQVAsQ0FBdUQsQ0FBQyxPQUF4RCxDQUFnRSxXQUFoRTtNQURrQyxDQUFwQztJQUpnQyxDQUFsQztXQVFBLFFBQUEsQ0FBUyx1QkFBVCxFQUFrQyxTQUFBO01BQ2hDLEVBQUEsQ0FBRyxnREFBSCxFQUFxRCxTQUFBO2VBQ25ELE1BQUEsQ0FBTyxVQUFVLENBQUMsbUJBQVgsQ0FBK0IsVUFBL0IsQ0FBUCxDQUFrRCxDQUFDLE9BQW5ELENBQTJELEVBQTNEO01BRG1ELENBQXJEO01BR0EsRUFBQSxDQUFHLG1FQUFILEVBQXdFLFNBQUE7QUFDdEUsWUFBQTtRQUFBLEtBQUEsQ0FBTSxFQUFOLEVBQVUsWUFBVixDQUF1QixDQUFDLFNBQXhCLENBQWtDLElBQWxDO1FBQ0EsS0FBQSxDQUFNLEVBQU4sRUFBVSxjQUFWLENBQXlCLENBQUMsU0FBMUIsQ0FBb0MsR0FBcEM7UUFDQSxLQUFBLENBQU0sYUFBTixFQUFxQixVQUFyQixDQUFnQyxDQUFDLGNBQWpDLENBQUE7UUFDQSxHQUFBLEdBQU0sVUFBVSxDQUFDLG1CQUFYLENBQUE7UUFDTixNQUFBLENBQU8sYUFBYSxDQUFDLFFBQXJCLENBQThCLENBQUMsZ0JBQS9CLENBQUE7ZUFDQSxNQUFBLENBQU8sR0FBUCxDQUFXLENBQUMsT0FBWixDQUFvQixFQUFwQjtNQU5zRSxDQUF4RTtNQVFBLEVBQUEsQ0FBRywyREFBSCxFQUFnRSxTQUFBO0FBQzlELFlBQUE7UUFBQSxLQUFBLENBQU0sRUFBTixFQUFVLFlBQVYsQ0FBdUIsQ0FBQyxTQUF4QixDQUFrQyxJQUFsQztRQUNBLEtBQUEsQ0FBTSxFQUFOLEVBQVUsY0FBVixDQUF5QixDQUFDLFNBQTFCLENBQW9DLElBQXBDO1FBQ0EsS0FBQSxDQUFNLGFBQU4sRUFBcUIsVUFBckIsQ0FBZ0MsQ0FBQyxjQUFqQyxDQUFBO1FBQ0EsR0FBQSxHQUFNLFVBQVUsQ0FBQyxtQkFBWCxDQUFBO1FBQ04sTUFBQSxDQUFPLGFBQWEsQ0FBQyxRQUFyQixDQUE4QixDQUFDLEdBQUcsQ0FBQyxnQkFBbkMsQ0FBQTtlQUNBLE1BQUEsQ0FBTyxHQUFQLENBQVcsQ0FBQyxPQUFaLENBQW9CLEVBQXBCO01BTjhELENBQWhFO01BU0EsUUFBQSxDQUFTLDBCQUFULEVBQXFDLFNBQUE7UUFDbkMsRUFBQSxDQUFHLHlEQUFILEVBQThELFNBQUE7QUFDNUQsY0FBQTtVQUFBLE1BQUEsR0FBUyxVQUFVLENBQUMsc0JBQVgsQ0FBQTtVQUNULGNBQUEsR0FDRTtZQUFBLFNBQUEsRUFBVyxDQUFDLENBQUQsRUFBRyxDQUFILENBQVg7WUFDQSxjQUFBLEVBQWdCLENBQUMsQ0FBRCxFQUFHLENBQUgsQ0FEaEI7WUFFQSx5QkFBQSxFQUEyQjtjQUFFLENBQUYsRUFBSztnQkFBRSxXQUFBLEVBQWEsYUFBZjtnQkFBOEIsUUFBQSxFQUFVLGFBQXhDO2VBQUw7YUFGM0I7O2lCQUdGLE1BQUEsQ0FBTyxNQUFQLENBQWMsQ0FBQyxPQUFmLENBQXVCLGNBQXZCO1FBTjRELENBQTlEO1FBUUEsRUFBQSxDQUFHLCtEQUFILEVBQW9FLFNBQUE7QUFDbEUsY0FBQTtVQUFBLE1BQUEsR0FBUyxVQUFVLENBQUMsc0JBQVgsQ0FBa0MsRUFBbEM7VUFDVCxjQUFBLEdBQ0U7WUFBQSxTQUFBLEVBQVcsQ0FBQyxDQUFELEVBQUcsQ0FBSCxDQUFYO1lBQ0EsY0FBQSxFQUFnQixDQUFDLENBQUQsRUFBRyxDQUFILENBRGhCO1lBRUEseUJBQUEsRUFBMkI7Y0FBRSxDQUFGLEVBQUs7Z0JBQUUsV0FBQSxFQUFhLGFBQWY7Z0JBQThCLFFBQUEsRUFBVSxhQUF4QztlQUFMO2FBRjNCOztpQkFHRixNQUFBLENBQU8sTUFBUCxDQUFjLENBQUMsT0FBZixDQUF1QixjQUF2QjtRQU5rRSxDQUFwRTtRQVFBLEVBQUEsQ0FBRyxxRkFBSCxFQUEwRixTQUFBO0FBQ3hGLGNBQUE7VUFBQSxLQUFBLEdBQ0U7WUFBQSxRQUFBLEVBQVUsQ0FBQyxDQUFELEVBQUksQ0FBSixDQUFWOztVQUNGLE1BQUEsR0FBUyxVQUFVLENBQUMsc0JBQVgsQ0FBa0MsS0FBbEM7VUFDVCxjQUFBLEdBQ0U7WUFBQSxTQUFBLEVBQVcsQ0FBQyxDQUFELEVBQUcsQ0FBSCxDQUFYO1lBQ0EsY0FBQSxFQUFnQixDQUFDLENBQUQsRUFBRyxDQUFILENBRGhCO1lBRUEseUJBQUEsRUFBMkI7Y0FBRSxDQUFGLEVBQUs7Z0JBQUUsV0FBQSxFQUFhLGFBQWY7Z0JBQThCLFFBQUEsRUFBVSxhQUF4QztlQUFMO2FBRjNCOztpQkFHRixNQUFBLENBQU8sTUFBUCxDQUFjLENBQUMsT0FBZixDQUF1QixjQUF2QjtRQVJ3RixDQUExRjtRQVVBLEVBQUEsQ0FBRywrRUFBSCxFQUFvRixTQUFBO0FBQ2xGLGNBQUE7VUFBQSxLQUFBLEdBQ0U7WUFBQSxRQUFBLEVBQVUsQ0FBQyxDQUFELEVBQUksS0FBSixDQUFWOztVQUNGLE1BQUEsR0FBUyxVQUFVLENBQUMsc0JBQVgsQ0FBa0MsS0FBbEM7VUFDVCxjQUFBLEdBQ0U7WUFBQSxTQUFBLEVBQVcsQ0FBQyxDQUFELEVBQUcsQ0FBSCxDQUFYO1lBQ0EsY0FBQSxFQUFnQixDQUFDLENBQUQsRUFBRyxDQUFILENBRGhCO1lBRUEseUJBQUEsRUFBMkI7Y0FBRSxDQUFGLEVBQUs7Z0JBQUUsV0FBQSxFQUFhLGFBQWY7Z0JBQThCLFFBQUEsRUFBVSxhQUF4QztlQUFMO2FBRjNCOztpQkFHRixNQUFBLENBQU8sTUFBUCxDQUFjLENBQUMsT0FBZixDQUF1QixjQUF2QjtRQVJrRixDQUFwRjtRQVVBLEVBQUEsQ0FBRywyREFBSCxFQUFnRSxTQUFBO0FBQzlELGNBQUE7VUFBQSxLQUFBLEdBQ0U7WUFBQSxRQUFBLEVBQVUsQ0FBQyxDQUFELEVBQUksQ0FBSixDQUFWO1lBQ0Esa0JBQUEsRUFBb0IsQ0FBQyxNQUFELEVBQVMsQ0FBVCxDQURwQjs7VUFFRixNQUFBLEdBQVMsVUFBVSxDQUFDLHNCQUFYLENBQWtDLEtBQWxDO1VBQ1QsY0FBQSxHQUNFO1lBQUEsU0FBQSxFQUFXLENBQUMsTUFBRCxFQUFTLENBQVQsQ0FBWDtZQUNBLGNBQUEsRUFBZ0IsQ0FBQyxDQUFELEVBQUksQ0FBSixDQURoQjtZQUVBLHlCQUFBLEVBQTJCO2NBQUUsQ0FBRixFQUFLO2dCQUFFLFdBQUEsRUFBYSxhQUFmO2dCQUE4QixRQUFBLEVBQVUsYUFBeEM7ZUFBTDthQUYzQjs7aUJBR0YsTUFBQSxDQUFPLE1BQVAsQ0FBYyxDQUFDLE9BQWYsQ0FBdUIsY0FBdkI7UUFUOEQsQ0FBaEU7UUFXQSxFQUFBLENBQUcsMkRBQUgsRUFBZ0UsU0FBQTtBQUM5RCxjQUFBO1VBQUEsS0FBQSxHQUNFO1lBQUEsUUFBQSxFQUFVLENBQUMsQ0FBRCxFQUFJLENBQUosQ0FBVjtZQUNBLGtCQUFBLEVBQW9CLENBQUMsTUFBRCxFQUFTLENBQVQsQ0FEcEI7WUFFQSx3QkFBQSxFQUEwQixDQUFDLENBQUQsRUFBSSxDQUFKLENBRjFCOztVQUdGLE1BQUEsR0FBUyxVQUFVLENBQUMsc0JBQVgsQ0FBa0MsS0FBbEM7VUFDVCxjQUFBLEdBQ0U7WUFBQSxTQUFBLEVBQVcsQ0FBQyxNQUFELEVBQVMsQ0FBVCxDQUFYO1lBQ0EsY0FBQSxFQUFnQixDQUFDLENBQUQsRUFBSSxDQUFKLENBRGhCO1lBRUEseUJBQUEsRUFBMkI7Y0FBRSxDQUFGLEVBQUs7Z0JBQUUsV0FBQSxFQUFhLGFBQWY7Z0JBQThCLFFBQUEsRUFBVSxhQUF4QztlQUFMO2FBRjNCOztpQkFHRixNQUFBLENBQU8sTUFBUCxDQUFjLENBQUMsT0FBZixDQUF1QixjQUF2QjtRQVY4RCxDQUFoRTtRQVlBLEVBQUEsQ0FBRyx5RUFBSCxFQUE4RSxTQUFBO0FBQzVFLGNBQUE7VUFBQSxLQUFBLEdBQ0U7WUFBQSxRQUFBLEVBQVUsQ0FBQyxDQUFELEVBQUksQ0FBSixDQUFWO1lBQ0Esa0JBQUEsRUFBb0IsQ0FBQyxNQUFELEVBQVMsQ0FBVCxDQURwQjtZQUVBLHdCQUFBLEVBQTBCLENBQUMsQ0FBRCxFQUFJLENBQUosQ0FGMUI7WUFHQSxvQ0FBQSxFQUFzQyxDQUFDLENBQUQsRUFBSSxjQUFKLENBSHRDOztVQUlGLE1BQUEsR0FBUyxVQUFVLENBQUMsc0JBQVgsQ0FBa0MsS0FBbEM7VUFDVCxjQUFBLEdBQ0U7WUFBQSxTQUFBLEVBQVcsQ0FBQyxNQUFELEVBQVMsQ0FBVCxDQUFYO1lBQ0EsY0FBQSxFQUFnQixDQUFDLENBQUQsRUFBSSxDQUFKLENBRGhCO1lBRUEseUJBQUEsRUFBMkI7Y0FBRSxDQUFGLEVBQUs7Z0JBQUUsV0FBQSxFQUFhLGNBQWY7Z0JBQStCLFFBQUEsRUFBVSxjQUF6QztlQUFMO2FBRjNCOztpQkFHRixNQUFBLENBQU8sTUFBUCxDQUFjLENBQUMsT0FBZixDQUF1QixjQUF2QjtRQVg0RSxDQUE5RTtlQWFBLEVBQUEsQ0FBRywyRkFBSCxFQUFnRyxTQUFBO0FBQzlGLGNBQUE7VUFBQSxLQUFBLEdBQ0U7WUFBQSxRQUFBLEVBQVUsQ0FBQyxDQUFELEVBQUksQ0FBSixDQUFWO1lBQ0Esa0JBQUEsRUFBb0IsQ0FBQyxNQUFELEVBQVMsQ0FBVCxDQURwQjtZQUVBLHdCQUFBLEVBQTBCLENBQUMsQ0FBRCxFQUFJLENBQUosQ0FGMUI7WUFHQSxvQ0FBQSxFQUFzQztjQUFFLENBQUYsRUFDcEM7Z0JBQUEsVUFBQSxFQUFZLGVBQVo7Z0JBQ0EsYUFBQSxFQUFlLGNBRGY7ZUFEb0M7YUFIdEM7O1VBT0YsTUFBQSxHQUFTLFVBQVUsQ0FBQyxzQkFBWCxDQUFrQyxLQUFsQztVQUNULGNBQUEsR0FDRTtZQUFBLFNBQUEsRUFBVyxDQUFDLE1BQUQsRUFBUyxDQUFULENBQVg7WUFDQSxjQUFBLEVBQWdCLENBQUMsQ0FBRCxFQUFJLENBQUosQ0FEaEI7WUFFQSx5QkFBQSxFQUEyQjtjQUFFLENBQUYsRUFBSztnQkFBRSxXQUFBLEVBQWEsY0FBZjtnQkFBK0IsUUFBQSxFQUFVLGVBQXpDO2VBQUw7YUFGM0I7O2lCQUdGLE1BQUEsQ0FBTyxNQUFQLENBQWMsQ0FBQyxPQUFmLENBQXVCLGNBQXZCO1FBZDhGLENBQWhHO01BekVtQyxDQUFyQztNQTBGQSxRQUFBLENBQVMsYUFBVCxFQUF3QixTQUFBO1FBRXRCLFVBQUEsQ0FBVyxTQUFBO1VBQ1QsVUFBQSxHQUFhO1VBc0NiLE1BQU0sQ0FBQyxVQUFQLENBQWtCLFVBQWxCO2lCQUNBLGVBQUEsR0FBc0IsSUFBQSxLQUFBLENBQVUsSUFBQSxLQUFBLENBQU0sQ0FBTixFQUFRLENBQVIsQ0FBVixFQUEwQixJQUFBLEtBQUEsQ0FBTSxFQUFOLEVBQVMsQ0FBVCxDQUExQjtRQXhDYixDQUFYO1FBMENBLEVBQUEsQ0FBRyw2Q0FBSCxFQUFrRCxTQUFBO0FBQ2hELGNBQUE7VUFBQSxZQUFBLEdBQWU7VUF1Q2YsVUFBVSxDQUFDLG1CQUFYLEdBQ0U7WUFBQSxTQUFBLEVBQVcsQ0FBQyxDQUFELEVBQUksQ0FBSixDQUFYO1lBQ0EsY0FBQSxFQUFnQixDQUFDLENBQUQsRUFBSSxDQUFKLENBRGhCO1lBRUEseUJBQUEsRUFBMkI7Y0FBRSxDQUFGLEVBQzFCO2dCQUFBLFdBQUEsRUFBYSxhQUFiO2dCQUNBLFFBQUEsRUFBVSxhQURWO2VBRDBCO2FBRjNCOztVQUtELFVBQVUsQ0FBQyxPQUFYLEdBQXFCO1VBQ3JCLFVBQVUsQ0FBQyxTQUFYLENBQXFCLGVBQXJCO2lCQUNBLE1BQUEsQ0FBTyxNQUFNLENBQUMsb0JBQVAsQ0FBNEIsZUFBNUIsQ0FBUCxDQUFvRCxDQUFDLE9BQXJELENBQTZELFlBQTdEO1FBaEQrQyxDQUFsRDtlQWtEQSxFQUFBLENBQUcsdUVBQUgsRUFBNEUsU0FBQTtBQUMxRSxjQUFBO1VBQUEsWUFBQSxHQUFlO1VBdUNmLFVBQVUsQ0FBQyxtQkFBWCxHQUNFO1lBQUEsU0FBQSxFQUFXLENBQUMsQ0FBRCxFQUFJLENBQUosQ0FBWDtZQUNBLGNBQUEsRUFBZ0IsQ0FBQyxDQUFELEVBQUksQ0FBSixDQURoQjtZQUVBLHlCQUFBLEVBQTJCO2NBQUUsQ0FBRixFQUN6QjtnQkFBQSxXQUFBLEVBQWEsZUFBYjtnQkFDQSxRQUFBLEVBQVUsZUFEVjtlQUR5QjthQUYzQjs7VUFLRCxVQUFVLENBQUMsT0FBWCxHQUFxQjtVQUNyQixVQUFVLENBQUMsU0FBWCxDQUFxQixlQUFyQjtpQkFDQSxNQUFBLENBQU8sTUFBTSxDQUFDLG9CQUFQLENBQTRCLGVBQTVCLENBQVAsQ0FBb0QsQ0FBQyxPQUFyRCxDQUE2RCxZQUE3RDtRQWhEeUUsQ0FBNUU7TUE5RnNCLENBQXhCO2FBaUpBLFFBQUEsQ0FBUyxlQUFULEVBQTBCLFNBQUE7ZUFFeEIsRUFBQSxDQUFHLGtFQUFILEVBQXVFLFNBQUE7VUFFckUsVUFBVSxDQUFDLG1CQUFYLEdBQ0U7WUFBQSxTQUFBLEVBQVcsQ0FBQyxDQUFELEVBQUksQ0FBSixDQUFYO1lBQ0EsY0FBQSxFQUFnQixDQUFDLENBQUQsRUFBSSxDQUFKLENBRGhCO1lBRUEseUJBQUEsRUFBMkI7Y0FBRSxDQUFGLEVBQ3pCO2dCQUFBLFdBQUEsRUFBYSxjQUFiO2dCQUNBLFFBQUEsRUFBVSxjQURWO2VBRHlCO2FBRjNCOztVQUtGLFVBQVUsQ0FBQyxPQUFYLEdBQXFCO1VBQ3JCLE1BQU0sQ0FBQyxVQUFQLENBQWtCLGFBQWxCO1VBQ0EsTUFBTSxDQUFDLHVCQUFQLENBQStCLENBQUMsQ0FBRCxFQUFHLENBQUgsQ0FBL0I7VUFDQSxNQUFNLENBQUMsVUFBUCxDQUFrQixJQUFsQjtVQUVBLE1BQUEsQ0FBTyxNQUFNLENBQUMsb0JBQVAsQ0FBNEIsQ0FBQyxDQUFDLENBQUQsRUFBRyxDQUFILENBQUQsRUFBTyxDQUFDLENBQUQsRUFBRyxDQUFILENBQVAsQ0FBNUIsQ0FBUCxDQUFrRCxDQUFDLE9BQW5ELENBQTJELE9BQTNEO1VBQ0EsTUFBQSxDQUFPLE1BQU0sQ0FBQyxvQkFBUCxDQUE0QixDQUFDLENBQUMsQ0FBRCxFQUFHLENBQUgsQ0FBRCxFQUFPLENBQUMsQ0FBRCxFQUFHLENBQUgsQ0FBUCxDQUE1QixDQUFQLENBQWtELENBQUMsT0FBbkQsQ0FBMkQsSUFBM0Q7VUFDQSxNQUFBLENBQU8sTUFBTSxDQUFDLG9CQUFQLENBQTRCLENBQUMsQ0FBQyxDQUFELEVBQUcsQ0FBSCxDQUFELEVBQU8sQ0FBQyxDQUFELEVBQUcsQ0FBSCxDQUFQLENBQTVCLENBQVAsQ0FBa0QsQ0FBQyxPQUFuRCxDQUEyRCxRQUEzRDtpQkFDQSxNQUFBLENBQU8sTUFBTSxDQUFDLHVCQUFQLENBQUEsQ0FBUCxDQUF3QyxDQUFDLE9BQXpDLENBQWlELENBQUMsQ0FBRCxFQUFHLENBQUgsQ0FBakQ7UUFoQnFFLENBQXZFO01BRndCLENBQTFCO0lBaFFnQyxDQUFsQztFQWxDc0IsQ0FBeEI7QUFMQSIsInNvdXJjZXNDb250ZW50IjpbIiMgVGVzdHMgZm9yIEF1dG8gSW5kZW50aW5nIEpTWFxuXG57UmFuZ2UsIFBvaW50fSA9IHJlcXVpcmUgJ2F0b20nXG5mcyA9IHJlcXVpcmUgJ2ZzLXBsdXMnXG5wYXRoID0gcmVxdWlyZSAncGF0aCdcbkF1dG9JbmRlbnQgPSByZXF1aXJlICcuLi9saWIvYXV0by1pbmRlbnQnXG5cbmRlc2NyaWJlICdhdXRvLWluZGVudCcsIC0+XG4gIFthdXRvSW5kZW50LCBlZGl0b3IsIG5vdGlmaWNhdGlvbnMsIHNvdXJjZUNvZGUsIHNvdXJjZUNvZGVSYW5nZV0gPSBbXVxuXG4gIGJlZm9yZUVhY2ggLT5cbiAgICB3YWl0c0ZvclByb21pc2UgLT5cbiAgICAgIGF0b20ucGFja2FnZXMuYWN0aXZhdGVQYWNrYWdlKCdsYW5ndWFnZS1iYWJlbCcpXG5cbiAgYmVmb3JlRWFjaCAtPlxuICAgIHdhaXRzRm9yUHJvbWlzZSAtPlxuICAgICAgYXRvbS53b3Jrc3BhY2Uub3Blbignbm9uLWV4aXN0ZW50LmpzJykudGhlbiAobykgLT4gZWRpdG9yID0gb1xuXG4gICAgcnVucyAtPlxuICAgICAgYXV0b0luZGVudCA9IG5ldyBBdXRvSW5kZW50KGVkaXRvcilcbiAgICAgIG5vdGlmaWNhdGlvbnMgPSBhdG9tLm5vdGlmaWNhdGlvbnNcblxuXG4gICMgOjogY29uc3RydWN0b3JcbiAgZGVzY3JpYmUgJzo6Y29uc3RydWN0b3InLCAtPlxuICAgIGl0ICcgc2hvdWxkIHNldHVwIHNvbWUgdmFsaWQgaW5kZW50YXRpb24gZGVmYXVsdHMnLCAtPlxuICAgICAgZXhwZWN0ZWRSZXN1bHQgPVxuICAgICAgICBqc3hJbmRlbnQ6IFsxLDFdXG4gICAgICAgIGpzeEluZGVudFByb3BzOiBbMSwxXVxuICAgICAgICBqc3hDbG9zaW5nQnJhY2tldExvY2F0aW9uOiBbIDEsIHsgc2VsZkNsb3Npbmc6ICd0YWctYWxpZ25lZCcsIG5vbkVtcHR5OiAndGFnLWFsaWduZWQnfSBdXG4gICAgICBleHBlY3QoYXV0b0luZGVudC5lc2xpbnRJbmRlbnRPcHRpb25zKS50b0VxdWFsKGV4cGVjdGVkUmVzdWx0KVxuXG4gICMgOjpnZXRFc2xpbnRyY0ZpbGVuYW1lXG4gIGRlc2NyaWJlICc6OmdldEVzbGludHJjRmlsZW5hbWUnLCAtPlxuICAgIGl0ICdyZXR1cm5zIGEgY29ycmVjdCBwcm9qZWN0IHBhdGggZm9yIHRoZSBzb3VyY2UgZmlsZScsIC0+XG4gICAgICBleHBlY3QocGF0aC5kaXJuYW1lKGF1dG9JbmRlbnQuZ2V0RXNsaW50cmNGaWxlbmFtZSgpKSkudG9FcXVhbChwYXRoLmRpcm5hbWUoZWRpdG9yLmdldFBhdGgoKSkpXG5cbiAgICBpdCAncmV0dXJucyBhIC5lc2xpbnRyYyBmaWxlIG5hbWUnLCAtPlxuICAgICAgZXhwZWN0KHBhdGguYmFzZW5hbWUoYXV0b0luZGVudC5nZXRFc2xpbnRyY0ZpbGVuYW1lKCkpKS50b0VxdWFsKCcuZXNsaW50cmMnKVxuXG4gICMgOjpyZWFkRXNsaW50cmNPcHRpb25zXG4gIGRlc2NyaWJlICc6OnJlYWRFc2xpbnRyY09wdGlvbnMnLCAtPlxuICAgIGl0ICdyZXR1cm5zIGFuIGVtcHR5IG9iamVjdCBvbiBhIG1pc3NpbmcgLmVzbGludHJjJywgLT5cbiAgICAgIGV4cGVjdChhdXRvSW5kZW50LnJlYWRFc2xpbnRyY09wdGlvbnMoJy5taXNzaW5nJykpLnRvRXF1YWwoe30pXG5cbiAgICBpdCAncmV0dXJucyBhbmQgZW1wdHkgT2JqZWN0IGFuZCBhIG5vdGlmaWNhdGlvbiBtZXNzYWdlIG9uIGJhZCBlc2xpbnQnLCAtPlxuICAgICAgc3B5T24oZnMsICdleGlzdHNTeW5jJykuYW5kUmV0dXJuKHRydWUpXG4gICAgICBzcHlPbihmcywgJ3JlYWRGaWxlU3luYycpLmFuZFJldHVybigneycpXG4gICAgICBzcHlPbihub3RpZmljYXRpb25zLCAnYWRkRXJyb3InKS5hbmRDYWxsVGhyb3VnaCgpXG4gICAgICBvYmogPSBhdXRvSW5kZW50LnJlYWRFc2xpbnRyY09wdGlvbnMoKVxuICAgICAgZXhwZWN0KG5vdGlmaWNhdGlvbnMuYWRkRXJyb3IpLnRvSGF2ZUJlZW5DYWxsZWQoKVxuICAgICAgZXhwZWN0KG9iaikudG9FcXVhbCh7fSlcblxuICAgIGl0ICdyZXR1cm5zIGFuIGVtcHR5IE9iamVjdCB3aGVuIGVzbGludCB3aXRoIG5vIHJ1bGVzIGlzIHJlYWQnLCAtPlxuICAgICAgc3B5T24oZnMsICdleGlzdHNTeW5jJykuYW5kUmV0dXJuKHRydWUpXG4gICAgICBzcHlPbihmcywgJ3JlYWRGaWxlU3luYycpLmFuZFJldHVybigne30nKVxuICAgICAgc3B5T24obm90aWZpY2F0aW9ucywgJ2FkZEVycm9yJykuYW5kQ2FsbFRocm91Z2goKVxuICAgICAgb2JqID0gYXV0b0luZGVudC5yZWFkRXNsaW50cmNPcHRpb25zKClcbiAgICAgIGV4cGVjdChub3RpZmljYXRpb25zLmFkZEVycm9yKS5ub3QudG9IYXZlQmVlbkNhbGxlZCgpXG4gICAgICBleHBlY3Qob2JqKS50b0VxdWFsKHt9KVxuXG4gICAgIyA6OnRyYW5zbGF0ZUluZGVudE9wdGlvbnNcbiAgICBkZXNjcmliZSAnOjp0cmFuc2xhdGVJbmRlbnRPcHRpb25zJywgLT5cbiAgICAgIGl0ICdzaG91bGQgcmV0dXJuIGV4cGVjdGVkIGRlZmF1bHRzIHdoZW4gbm8gb2JqZWN0IGlzIGlucHV0JywgLT5cbiAgICAgICAgcmVzdWx0ID0gYXV0b0luZGVudC50cmFuc2xhdGVJbmRlbnRPcHRpb25zKClcbiAgICAgICAgZXhwZWN0ZWRSZXN1bHQgPVxuICAgICAgICAgIGpzeEluZGVudDogWzEsMV1cbiAgICAgICAgICBqc3hJbmRlbnRQcm9wczogWzEsMV1cbiAgICAgICAgICBqc3hDbG9zaW5nQnJhY2tldExvY2F0aW9uOiBbIDEsIHsgc2VsZkNsb3Npbmc6ICd0YWctYWxpZ25lZCcsIG5vbkVtcHR5OiAndGFnLWFsaWduZWQnfSBdXG4gICAgICAgIGV4cGVjdChyZXN1bHQpLnRvRXF1YWwoZXhwZWN0ZWRSZXN1bHQpXG5cbiAgICAgIGl0ICdzaG91bGQgcmV0dXJuIGV4cGVjdGVkIGRlZmF1bHRzIHdoZW4gbm8gdmFsaWQgb2JqZWN0IGlzIGlucHV0JywgLT5cbiAgICAgICAgcmVzdWx0ID0gYXV0b0luZGVudC50cmFuc2xhdGVJbmRlbnRPcHRpb25zKHt9KVxuICAgICAgICBleHBlY3RlZFJlc3VsdCA9XG4gICAgICAgICAganN4SW5kZW50OiBbMSwxXVxuICAgICAgICAgIGpzeEluZGVudFByb3BzOiBbMSwxXVxuICAgICAgICAgIGpzeENsb3NpbmdCcmFja2V0TG9jYXRpb246IFsgMSwgeyBzZWxmQ2xvc2luZzogJ3RhZy1hbGlnbmVkJywgbm9uRW1wdHk6ICd0YWctYWxpZ25lZCd9IF1cbiAgICAgICAgZXhwZWN0KHJlc3VsdCkudG9FcXVhbChleHBlY3RlZFJlc3VsdClcblxuICAgICAgaXQgJ3Nob3VsZCByZXR1cm4gdHdvIHRhYiBtYXJrZXJzIGZvciBqc3ggYW5kIHByb3BzIHdoZW4gYW4gaW5kZW50IG9mIDQgc3BhY2VzIGlzIGZvdW5kJywgLT5cbiAgICAgICAgcnVsZXMgPVxuICAgICAgICAgIFwiaW5kZW50XCI6IFsxLCA0XVxuICAgICAgICByZXN1bHQgPSBhdXRvSW5kZW50LnRyYW5zbGF0ZUluZGVudE9wdGlvbnMocnVsZXMpXG4gICAgICAgIGV4cGVjdGVkUmVzdWx0ID1cbiAgICAgICAgICBqc3hJbmRlbnQ6IFsxLDJdXG4gICAgICAgICAganN4SW5kZW50UHJvcHM6IFsxLDJdXG4gICAgICAgICAganN4Q2xvc2luZ0JyYWNrZXRMb2NhdGlvbjogWyAxLCB7IHNlbGZDbG9zaW5nOiAndGFnLWFsaWduZWQnLCBub25FbXB0eTogJ3RhZy1hbGlnbmVkJ30gXVxuICAgICAgICBleHBlY3QocmVzdWx0KS50b0VxdWFsKGV4cGVjdGVkUmVzdWx0KVxuXG4gICAgICBpdCAnc2hvdWxkIHJldHVybiBvbmUgdGFiIG1hcmtlcnMgZm9yIGpzeCBhbmQgcHJvcHMgd2hlbiBhbiBpbmRlbnQgXCJ0YWJcIiBpcyBmb3VuZCcsIC0+XG4gICAgICAgIHJ1bGVzID1cbiAgICAgICAgICBcImluZGVudFwiOiBbMSwgXCJ0YWJcIl1cbiAgICAgICAgcmVzdWx0ID0gYXV0b0luZGVudC50cmFuc2xhdGVJbmRlbnRPcHRpb25zKHJ1bGVzKVxuICAgICAgICBleHBlY3RlZFJlc3VsdCA9XG4gICAgICAgICAganN4SW5kZW50OiBbMSwxXVxuICAgICAgICAgIGpzeEluZGVudFByb3BzOiBbMSwxXVxuICAgICAgICAgIGpzeENsb3NpbmdCcmFja2V0TG9jYXRpb246IFsgMSwgeyBzZWxmQ2xvc2luZzogJ3RhZy1hbGlnbmVkJywgbm9uRW1wdHk6ICd0YWctYWxpZ25lZCd9IF1cbiAgICAgICAgZXhwZWN0KHJlc3VsdCkudG9FcXVhbChleHBlY3RlZFJlc3VsdClcblxuICAgICAgaXQgJ3Nob3VsZCByZXR1cm4ganN4SW5kZW50IG9mIDIgdGFicyBhbmQganN4SW5kZW50UHJvcHMgb2YgMycsIC0+XG4gICAgICAgIHJ1bGVzID1cbiAgICAgICAgICBcImluZGVudFwiOiBbMSwgNl1cbiAgICAgICAgICBcInJlYWN0L2pzeC1pbmRlbnRcIjogW1wid2FyblwiLCA0XVxuICAgICAgICByZXN1bHQgPSBhdXRvSW5kZW50LnRyYW5zbGF0ZUluZGVudE9wdGlvbnMocnVsZXMpXG4gICAgICAgIGV4cGVjdGVkUmVzdWx0ID1cbiAgICAgICAgICBqc3hJbmRlbnQ6IFsnd2FybicsIDJdXG4gICAgICAgICAganN4SW5kZW50UHJvcHM6IFsxLCAzXVxuICAgICAgICAgIGpzeENsb3NpbmdCcmFja2V0TG9jYXRpb246IFsgMSwgeyBzZWxmQ2xvc2luZzogJ3RhZy1hbGlnbmVkJywgbm9uRW1wdHk6ICd0YWctYWxpZ25lZCd9IF1cbiAgICAgICAgZXhwZWN0KHJlc3VsdCkudG9FcXVhbChleHBlY3RlZFJlc3VsdClcblxuICAgICAgaXQgJ3Nob3VsZCByZXR1cm4ganN4SW5kZW50IG9mIDIgdGFicyBhbmQganN4SW5kZW50UHJvcHMgb2YgMicsIC0+XG4gICAgICAgIHJ1bGVzID1cbiAgICAgICAgICBcImluZGVudFwiOiBbMSwgNl1cbiAgICAgICAgICBcInJlYWN0L2pzeC1pbmRlbnRcIjogW1wid2FyblwiLCA0XVxuICAgICAgICAgIFwicmVhY3QvanN4LWluZGVudC1wcm9wc1wiOiBbMiwgNF1cbiAgICAgICAgcmVzdWx0ID0gYXV0b0luZGVudC50cmFuc2xhdGVJbmRlbnRPcHRpb25zKHJ1bGVzKVxuICAgICAgICBleHBlY3RlZFJlc3VsdCA9XG4gICAgICAgICAganN4SW5kZW50OiBbJ3dhcm4nLCAyXVxuICAgICAgICAgIGpzeEluZGVudFByb3BzOiBbMiwgMl1cbiAgICAgICAgICBqc3hDbG9zaW5nQnJhY2tldExvY2F0aW9uOiBbIDEsIHsgc2VsZkNsb3Npbmc6ICd0YWctYWxpZ25lZCcsIG5vbkVtcHR5OiAndGFnLWFsaWduZWQnfSBdXG4gICAgICAgIGV4cGVjdChyZXN1bHQpLnRvRXF1YWwoZXhwZWN0ZWRSZXN1bHQpXG5cbiAgICAgIGl0ICdzaG91bGQgcmV0dXJuIGpzeEluZGVudCBvZiAyIHRhYnMgYW5kIGpzeEluZGVudFByb3BzIG9mIDIsIGxpbmUtYWxpZ25lZCcsIC0+XG4gICAgICAgIHJ1bGVzID1cbiAgICAgICAgICBcImluZGVudFwiOiBbMSwgNl1cbiAgICAgICAgICBcInJlYWN0L2pzeC1pbmRlbnRcIjogW1wid2FyblwiLCA0XVxuICAgICAgICAgIFwicmVhY3QvanN4LWluZGVudC1wcm9wc1wiOiBbMiwgNF1cbiAgICAgICAgICAncmVhY3QvanN4LWNsb3NpbmctYnJhY2tldC1sb2NhdGlvbic6IFsxLCAnbGluZS1hbGlnbmVkJ11cbiAgICAgICAgcmVzdWx0ID0gYXV0b0luZGVudC50cmFuc2xhdGVJbmRlbnRPcHRpb25zKHJ1bGVzKVxuICAgICAgICBleHBlY3RlZFJlc3VsdCA9XG4gICAgICAgICAganN4SW5kZW50OiBbJ3dhcm4nLCAyXVxuICAgICAgICAgIGpzeEluZGVudFByb3BzOiBbMiwgMl1cbiAgICAgICAgICBqc3hDbG9zaW5nQnJhY2tldExvY2F0aW9uOiBbIDEsIHsgc2VsZkNsb3Npbmc6ICdsaW5lLWFsaWduZWQnLCBub25FbXB0eTogJ2xpbmUtYWxpZ25lZCd9IF1cbiAgICAgICAgZXhwZWN0KHJlc3VsdCkudG9FcXVhbChleHBlY3RlZFJlc3VsdClcblxuICAgICAgaXQgJ3Nob3VsZCByZXR1cm4ganN4SW5kZW50IG9mIDIgdGFicyBhbmQganN4SW5kZW50UHJvcHMgb2YgMiwgbGluZS1hbGlnbmVkIGFuZCBwcm9wcy1hbGlnbmVkJywgLT5cbiAgICAgICAgcnVsZXMgPVxuICAgICAgICAgIFwiaW5kZW50XCI6IFsxLCA2XVxuICAgICAgICAgIFwicmVhY3QvanN4LWluZGVudFwiOiBbXCJ3YXJuXCIsIDRdXG4gICAgICAgICAgXCJyZWFjdC9qc3gtaW5kZW50LXByb3BzXCI6IFsyLCA0XVxuICAgICAgICAgIFwicmVhY3QvanN4LWNsb3NpbmctYnJhY2tldC1sb2NhdGlvblwiOiBbIDEsXG4gICAgICAgICAgICBcIm5vbkVtcHR5XCI6IFwicHJvcHMtYWxpZ25lZFwiLFxuICAgICAgICAgICAgXCJzZWxmQ2xvc2luZ1wiOiBcImxpbmUtYWxpZ25lZFwiXG4gICAgICAgICAgXVxuICAgICAgICByZXN1bHQgPSBhdXRvSW5kZW50LnRyYW5zbGF0ZUluZGVudE9wdGlvbnMocnVsZXMpXG4gICAgICAgIGV4cGVjdGVkUmVzdWx0ID1cbiAgICAgICAgICBqc3hJbmRlbnQ6IFsnd2FybicsIDJdXG4gICAgICAgICAganN4SW5kZW50UHJvcHM6IFsyLCAyXVxuICAgICAgICAgIGpzeENsb3NpbmdCcmFja2V0TG9jYXRpb246IFsgMSwgeyBzZWxmQ2xvc2luZzogJ2xpbmUtYWxpZ25lZCcsIG5vbkVtcHR5OiAncHJvcHMtYWxpZ25lZCd9IF1cbiAgICAgICAgZXhwZWN0KHJlc3VsdCkudG9FcXVhbChleHBlY3RlZFJlc3VsdClcblxuICAgICM6IGluZGVudEpTWFxuICAgIGRlc2NyaWJlICc6OmluZGVudEpTWCcsIC0+XG5cbiAgICAgIGJlZm9yZUVhY2ggLT5cbiAgICAgICAgc291cmNlQ29kZSA9IFwiXCJcIlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtyb290Q2xhc3N9PlxuICAgICAgICAgIHt0aGlzLl9yZW5kZXJQbGFjZWhvbGRlcigpfVxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICBjbGFzc05hbWU9e2N4KCdEcmFmdEVkaXRvci9lZGl0b3JDb250YWluZXInKX1cbiAgICAgICAgICBrZXk9eydlZGl0b3InICsgdGhpcy5zdGF0ZS5jb250YWluZXJLZXl9XG4gICAgICAgICAgcmVmPVwiZWRpdG9yQ29udGFpbmVyXCJcbiAgICAgICAgICA+XG4gICAgICAgICAgPGRpdlxuICAgICAgICAgIGFyaWEtYWN0aXZlZGVzY2VuZGFudD17XG4gICAgICAgICAgcmVhZE9ubHkgPyBudWxsIDogdGhpcy5wcm9wcy5hcmlhQWN0aXZlRGVzY2VuZGFudElEXG4gICAgICAgICAgfVxuICAgICAgICAgIGFyaWEtYXV0b2NvbXBsZXRlPXtyZWFkT25seSA/IG51bGwgOiB0aGlzLnByb3BzLmFyaWFBdXRvQ29tcGxldGV9XG4gICAgICAgICAgPlxuICAgICAgICAgIHt0aGlzLl9yZW5kZXJQbGFjZWhvbGRlcigpfVxuICAgICAgICAgIDxDb21wb25lbnQgcDFcbiAgICAgICAgICBwMlxuICAgICAgICAgIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgeyAvLyB0ZXN0cyBpbmxpbmUgSlNYXG4gICAgICAgICAgaWYgKGEpIHtcbiAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgIDxkaXY+PC9kaXY+XG4gICAgICAgICAgKVxuICAgICAgICAgIH1cbiAgICAgICAgICBlbHNlIChiKSB7XG4gICAgICAgICAgc3dpdGNoIChhKSB7XG4gICAgICAgICAgY2FzZSAxOlxuICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgPGRpdj48L2Rpdj5cbiAgICAgICAgICApXG4gICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICBcIlwiXCJcbiAgICAgICAgZWRpdG9yLmluc2VydFRleHQoc291cmNlQ29kZSlcbiAgICAgICAgc291cmNlQ29kZVJhbmdlID0gbmV3IFJhbmdlKG5ldyBQb2ludCgwLDApLCBuZXcgUG9pbnQoMzUsNikpXG5cbiAgICAgIGl0ICdzaG91bGQgaW5kZW50IEpTWCBhY2NvcmRpbmcgdG8gZXNsaW50IHJ1bGVzJywgLT5cbiAgICAgICAgaW5kZW50ZWRDb2RlID0gXCJcIlwiXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e3Jvb3RDbGFzc30+XG4gICAgICAgICAgICAgIHt0aGlzLl9yZW5kZXJQbGFjZWhvbGRlcigpfVxuICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2N4KCdEcmFmdEVkaXRvci9lZGl0b3JDb250YWluZXInKX1cbiAgICAgICAgICAgICAgICAgIGtleT17J2VkaXRvcicgKyB0aGlzLnN0YXRlLmNvbnRhaW5lcktleX1cbiAgICAgICAgICAgICAgICAgIHJlZj1cImVkaXRvckNvbnRhaW5lclwiXG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgICBhcmlhLWFjdGl2ZWRlc2NlbmRhbnQ9e1xuICAgICAgICAgICAgICAgICAgICAgICAgICByZWFkT25seSA/IG51bGwgOiB0aGlzLnByb3BzLmFyaWFBY3RpdmVEZXNjZW5kYW50SURcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgYXJpYS1hdXRvY29tcGxldGU9e3JlYWRPbmx5ID8gbnVsbCA6IHRoaXMucHJvcHMuYXJpYUF1dG9Db21wbGV0ZX1cbiAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICB7dGhpcy5fcmVuZGVyUGxhY2Vob2xkZXIoKX1cbiAgICAgICAgICAgICAgICAgICAgICA8Q29tcG9uZW50IHAxXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHAyXG4gICAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgICAgeyAvLyB0ZXN0cyBpbmxpbmUgSlNYXG4gICAgICAgICAgICAgICAgICAgICAgaWYgKGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgZWxzZSAoYikge1xuICAgICAgICAgICAgICAgICAgICAgICAgICBzd2l0Y2ggKGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhc2UgMTpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8ZGl2PjwvZGl2PlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIFwiXCJcIlxuICAgICAgICAjIHJlbWVtYmVyIHRoaXMgaXMgdGFicyBiYXNlZCBvbiBhdG9tIGRlZmF1bHRcbiAgICAgICAgYXV0b0luZGVudC5lc2xpbnRJbmRlbnRPcHRpb25zID1cbiAgICAgICAgICBqc3hJbmRlbnQ6IFsxLCAyXVxuICAgICAgICAgIGpzeEluZGVudFByb3BzOiBbMSwgMl1cbiAgICAgICAgICBqc3hDbG9zaW5nQnJhY2tldExvY2F0aW9uOiBbIDEsXG4gICAgICAgICAgIHNlbGZDbG9zaW5nOiAndGFnLWFsaWduZWQnXG4gICAgICAgICAgIG5vbkVtcHR5OiAndGFnLWFsaWduZWQnIF1cbiAgICAgICAgIGF1dG9JbmRlbnQuYXV0b0pzeCA9IHRydWVcbiAgICAgICAgIGF1dG9JbmRlbnQuaW5kZW50SlNYKHNvdXJjZUNvZGVSYW5nZSlcbiAgICAgICAgIGV4cGVjdChlZGl0b3IuZ2V0VGV4dEluQnVmZmVyUmFuZ2Uoc291cmNlQ29kZVJhbmdlKSkudG9FcXVhbChpbmRlbnRlZENvZGUpXG5cbiAgICAgIGl0ICdzaG91bGQgaW5kZW50IEpTWCBhY2NvcmRpbmcgdG8gZXNsaW50IHJ1bGVzIGFuZCB0YWcgY2xvc2luZyBhbGlnbm1lbnQnLCAtPlxuICAgICAgICBpbmRlbnRlZENvZGUgPSBcIlwiXCJcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17cm9vdENsYXNzfT5cbiAgICAgICAgICAgICAge3RoaXMuX3JlbmRlclBsYWNlaG9sZGVyKCl9XG4gICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y3goJ0RyYWZ0RWRpdG9yL2VkaXRvckNvbnRhaW5lcicpfVxuICAgICAgICAgICAgICAgICAga2V5PXsnZWRpdG9yJyArIHRoaXMuc3RhdGUuY29udGFpbmVyS2V5fVxuICAgICAgICAgICAgICAgICAgcmVmPVwiZWRpdG9yQ29udGFpbmVyXCJcbiAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgICAgICBhcmlhLWFjdGl2ZWRlc2NlbmRhbnQ9e1xuICAgICAgICAgICAgICAgICAgICAgICAgICByZWFkT25seSA/IG51bGwgOiB0aGlzLnByb3BzLmFyaWFBY3RpdmVEZXNjZW5kYW50SURcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgYXJpYS1hdXRvY29tcGxldGU9e3JlYWRPbmx5ID8gbnVsbCA6IHRoaXMucHJvcHMuYXJpYUF1dG9Db21wbGV0ZX1cbiAgICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAge3RoaXMuX3JlbmRlclBsYWNlaG9sZGVyKCl9XG4gICAgICAgICAgICAgICAgICAgICAgPENvbXBvbmVudCBwMVxuICAgICAgICAgICAgICAgICAgICAgICAgICBwMlxuICAgICAgICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgICB7IC8vIHRlc3RzIGlubGluZSBKU1hcbiAgICAgICAgICAgICAgICAgICAgICBpZiAoYSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gKFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGRpdj48L2Rpdj5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICBlbHNlIChiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIHN3aXRjaCAoYSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2FzZSAxOlxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiAoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxkaXY+PC9kaXY+XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgXCJcIlwiXG4gICAgICAgICMgcmVtZW1iZXIgdGhpcyBpcyB0YWJzIGJhc2VkIG9uIGF0b20gZGVmYXVsdFxuICAgICAgICBhdXRvSW5kZW50LmVzbGludEluZGVudE9wdGlvbnMgPVxuICAgICAgICAgIGpzeEluZGVudDogWzEsIDJdXG4gICAgICAgICAganN4SW5kZW50UHJvcHM6IFsxLCAyXVxuICAgICAgICAgIGpzeENsb3NpbmdCcmFja2V0TG9jYXRpb246IFsgMSxcbiAgICAgICAgICAgIHNlbGZDbG9zaW5nOiAncHJvcHMtYWxpZ25lZCdcbiAgICAgICAgICAgIG5vbkVtcHR5OiAncHJvcHMtYWxpZ25lZCcgXVxuICAgICAgICAgYXV0b0luZGVudC5hdXRvSnN4ID0gdHJ1ZVxuICAgICAgICAgYXV0b0luZGVudC5pbmRlbnRKU1goc291cmNlQ29kZVJhbmdlKVxuICAgICAgICAgZXhwZWN0KGVkaXRvci5nZXRUZXh0SW5CdWZmZXJSYW5nZShzb3VyY2VDb2RlUmFuZ2UpKS50b0VxdWFsKGluZGVudGVkQ29kZSlcblxuICAgICMgdGVzdCBpbnNlcnQgbmV3bGluZSBiZXR3ZWVuIG9wZW5pbmcgY2xvc2luZyBKU1ggdGFnc1xuICAgIGRlc2NyaWJlICdpbnNlcnQtbmwtanN4JywgLT5cblxuICAgICAgaXQgJ3Nob3VsZCBpbnNlcnQgdHdvIG5ldyBsaW5lcyBhbmQgcG9zaXRpb24gY3Vyc29yIGJldHdlZW4gSlNYIHRhZ3MnLCAtPlxuICAgICAgICAjIHJlbWVtYmVyIHRoaXMgaXMgdGFicyBiYXNlZCBvbiBhdG9tIGRlZmF1bHRcbiAgICAgICAgYXV0b0luZGVudC5lc2xpbnRJbmRlbnRPcHRpb25zID1cbiAgICAgICAgICBqc3hJbmRlbnQ6IFsxLCAxXVxuICAgICAgICAgIGpzeEluZGVudFByb3BzOiBbMSwgMV1cbiAgICAgICAgICBqc3hDbG9zaW5nQnJhY2tldExvY2F0aW9uOiBbIDEsXG4gICAgICAgICAgICBzZWxmQ2xvc2luZzogJ3RhYnMtYWxpZ25lZCdcbiAgICAgICAgICAgIG5vbkVtcHR5OiAndGFicy1hbGlnbmVkJyBdXG4gICAgICAgIGF1dG9JbmRlbnQuYXV0b0pzeCA9IHRydWVcbiAgICAgICAgZWRpdG9yLmluc2VydFRleHQoJzxkaXY+PC9kaXY+JylcbiAgICAgICAgZWRpdG9yLnNldEN1cnNvckJ1ZmZlclBvc2l0aW9uKFswLDVdKVxuICAgICAgICBlZGl0b3IuaW5zZXJ0VGV4dCgnXFxuJylcblxuICAgICAgICBleHBlY3QoZWRpdG9yLmdldFRleHRJbkJ1ZmZlclJhbmdlKFtbMCwwXSxbMCw1XV0pKS50b0VxdWFsKFwiPGRpdj5cIilcbiAgICAgICAgZXhwZWN0KGVkaXRvci5nZXRUZXh0SW5CdWZmZXJSYW5nZShbWzEsMF0sWzEsMl1dKSkudG9FcXVhbChcIiAgXCIpXG4gICAgICAgIGV4cGVjdChlZGl0b3IuZ2V0VGV4dEluQnVmZmVyUmFuZ2UoW1syLDBdLFsyLDZdXSkpLnRvRXF1YWwoXCI8L2Rpdj5cIilcbiAgICAgICAgZXhwZWN0KGVkaXRvci5nZXRDdXJzb3JCdWZmZXJQb3NpdGlvbigpKS50b0VxdWFsKFsxLDJdKVxuIl19
