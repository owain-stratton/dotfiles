Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj['default'] = obj; return newObj; } }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i]; return arr2; } else { return Array.from(arr); } }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { var callNext = step.bind(null, 'next'); var callThrow = step.bind(null, 'throw'); function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(callNext, callThrow); } } callNext(); }); }; }

// eslint-disable-next-line import/no-extraneous-dependencies, import/extensions

var _atom = require('atom');

var _atomLinter = require('atom-linter');

var helpers = _interopRequireWildcard(_atomLinter);

var _path = require('path');

// Settings
'use babel';var executablePath = undefined;
var additionalArguments = undefined;
var disableOnNoConfig = undefined;
var configName = undefined;
var disableTimeout = undefined;

exports['default'] = {
  activate: function activate() {
    require('atom-package-deps').install('linter-scss-lint');

    this.subs = new _atom.CompositeDisposable();
    this.subs.add(atom.config.observe('linter-scss-lint.executablePath', function (value) {
      executablePath = value;
    }));
    this.subs.add(atom.config.observe('linter-scss-lint.additionalArguments', function (value) {
      additionalArguments = value;
    }));
    this.subs.add(atom.config.observe('linter-scss-lint.disableWhenNoConfigFileInPath', function (value) {
      disableOnNoConfig = value;
    }));
    this.subs.add(atom.config.observe('linter-scss-lint.configName', function (value) {
      configName = value;
    }));
    this.subs.add(atom.config.observe('linter-scss-lint.disableTimeout', function (value) {
      disableTimeout = value;
    }));
  },

  deactivate: function deactivate() {
    this.subs.dispose();
  },

  getRelativeFilePath: function getRelativeFilePath(filePath, configPath) {
    if (configPath) {
      return (0, _path.relative)((0, _path.dirname)(configPath), filePath);
    }
    return filePath;
  },

  provideLinter: function provideLinter() {
    var _this = this;

    return {
      name: 'scss-lint',
      grammarScopes: ['source.css.scss', 'source.scss'],
      scope: 'file',
      lintOnFly: true,
      lint: _asyncToGenerator(function* (editor) {
        var filePath = editor.getPath();
        var fileText = editor.getText();

        if (fileText.length === 0) {
          return [];
        }

        var config = yield helpers.findAsync(filePath, configName);
        var relativeFilePath = _this.getRelativeFilePath(filePath, config);

        if (disableOnNoConfig && !config) {
          return [];
        }

        var options = {
          cwd: (0, _path.dirname)(filePath),
          stdin: fileText,
          ignoreExitCode: true
        };
        if (disableTimeout) {
          options.timeout = Infinity;
        }

        var params = ['--stdin-file-path=' + relativeFilePath, '--format=JSON', config != null ? '--config=' + config : undefined].concat(_toConsumableArray(additionalArguments.split(' '))).filter(function (e) {
          return e;
        });

        var output = yield helpers.exec(executablePath, params, options);

        if (editor.getText() !== fileText) {
          // Editor contents have changed, don't update messages
          return null;
        }

        var contents = undefined;
        try {
          contents = JSON.parse(output);
        } catch (error) {
          var regex1 = /^invalid option: --stdin-file-path=/; // <0.43.0
          var regex2 = /^undefined local variable or method `file'/; // 0.43.0, 0.43.1
          if (regex1.exec(output) || regex2.exec(output)) {
            atom.notifications.addError('You are using an old version of scss-lint', {
              detail: 'Please upgrade your version of scss-lint.\nCheck the README for further information.',
              dismissable: true
            });
            // Tell Linter not to update the current messages (if any)
            return null;
          }
          // eslint-disable-next-line no-console
          console.error('[Linter-SCSS-Lint]', error, output);
          atom.notifications.addError('[Linter-SCSS-Lint]', {
            detail: 'SCSS-Lint returned an invalid response, check your console for more info.',
            dismissable: true
          });
          // Tell Linter not to update the current messages (if any)
          return null;
        }

        return (contents[relativeFilePath] || []).map(function (msg) {
          var badge = undefined;
          if (msg.linter) {
            badge = '<span class=\'badge badge-flexible scss-lint\'>' + msg.linter + '</span> ';
          }

          // Atom expects ranges to be 0-based
          var line = (msg.line || 1) - 1;
          var col = (msg.column || 1) - 1;
          var range = undefined;

          if (msg.length) {
            // If the message defines a length, use that
            range = [[line, col], [line, col + msg.length]];
          } else {
            // Otherwise generate a range for the next "word" as defined by the language
            // NOTE: Current versions of scss-lint should _never_ hit this
            helpers.generateRange(editor, line, msg.column ? msg.column - 1 : undefined);
          }

          return {
            type: msg.severity || 'error',
            html: '' + (badge || '') + (msg.reason || 'Unknown Error'),
            filePath: filePath,
            range: range
          };
        });
      })
    };
  }
};
module.exports = exports['default'];
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9vd2Fpbi8uYXRvbS9wYWNrYWdlcy9saW50ZXItc2Nzcy1saW50L2xpYi9pbml0LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7OztvQkFHb0MsTUFBTTs7MEJBQ2pCLGFBQWE7O0lBQTFCLE9BQU87O29CQUNlLE1BQU07OztBQUx4QyxXQUFXLENBQUMsQUFRWixJQUFJLGNBQWMsWUFBQSxDQUFDO0FBQ25CLElBQUksbUJBQW1CLFlBQUEsQ0FBQztBQUN4QixJQUFJLGlCQUFpQixZQUFBLENBQUM7QUFDdEIsSUFBSSxVQUFVLFlBQUEsQ0FBQztBQUNmLElBQUksY0FBYyxZQUFBLENBQUM7O3FCQUVKO0FBQ2IsVUFBUSxFQUFBLG9CQUFHO0FBQ1QsV0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLENBQUM7O0FBRXpELFFBQUksQ0FBQyxJQUFJLEdBQUcsK0JBQXlCLENBQUM7QUFDdEMsUUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQ1gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsaUNBQWlDLEVBQUUsVUFBQyxLQUFLLEVBQUs7QUFDaEUsb0JBQWMsR0FBRyxLQUFLLENBQUM7S0FDeEIsQ0FDRixDQUNBLENBQUM7QUFDRixRQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FDWCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxzQ0FBc0MsRUFBRSxVQUFDLEtBQUssRUFBSztBQUNyRSx5QkFBbUIsR0FBRyxLQUFLLENBQUM7S0FDN0IsQ0FBQyxDQUNILENBQUM7QUFDRixRQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FDWCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxnREFBZ0QsRUFBRSxVQUFDLEtBQUssRUFBSztBQUMvRSx1QkFBaUIsR0FBRyxLQUFLLENBQUM7S0FDM0IsQ0FBQyxDQUNILENBQUM7QUFDRixRQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FDWCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyw2QkFBNkIsRUFBRSxVQUFDLEtBQUssRUFBSztBQUM1RCxnQkFBVSxHQUFHLEtBQUssQ0FBQztLQUNwQixDQUFDLENBQ0gsQ0FBQztBQUNGLFFBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUNYLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLGlDQUFpQyxFQUFFLFVBQUMsS0FBSyxFQUFLO0FBQ2hFLG9CQUFjLEdBQUcsS0FBSyxDQUFDO0tBQ3hCLENBQUMsQ0FDSCxDQUFDO0dBQ0g7O0FBRUQsWUFBVSxFQUFBLHNCQUFHO0FBQ1gsUUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztHQUNyQjs7QUFFRCxxQkFBbUIsRUFBQSw2QkFBQyxRQUFRLEVBQUUsVUFBVSxFQUFFO0FBQ3hDLFFBQUksVUFBVSxFQUFFO0FBQ2QsYUFBTyxvQkFBUyxtQkFBUSxVQUFVLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQztLQUNoRDtBQUNELFdBQU8sUUFBUSxDQUFDO0dBQ2pCOztBQUVELGVBQWEsRUFBQSx5QkFBRzs7O0FBQ2QsV0FBTztBQUNMLFVBQUksRUFBRSxXQUFXO0FBQ2pCLG1CQUFhLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSxhQUFhLENBQUM7QUFDakQsV0FBSyxFQUFFLE1BQU07QUFDYixlQUFTLEVBQUUsSUFBSTtBQUNmLFVBQUksb0JBQUUsV0FBTyxNQUFNLEVBQUs7QUFDdEIsWUFBTSxRQUFRLEdBQUcsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO0FBQ2xDLFlBQU0sUUFBUSxHQUFHLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQzs7QUFFbEMsWUFBSSxRQUFRLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtBQUN6QixpQkFBTyxFQUFFLENBQUM7U0FDWDs7QUFFRCxZQUFNLE1BQU0sR0FBRyxNQUFNLE9BQU8sQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0FBQzdELFlBQU0sZ0JBQWdCLEdBQUcsTUFBSyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7O0FBRXBFLFlBQUksaUJBQWlCLElBQUksQ0FBQyxNQUFNLEVBQUU7QUFDaEMsaUJBQU8sRUFBRSxDQUFDO1NBQ1g7O0FBRUQsWUFBTSxPQUFPLEdBQUc7QUFDZCxhQUFHLEVBQUUsbUJBQVEsUUFBUSxDQUFDO0FBQ3RCLGVBQUssRUFBRSxRQUFRO0FBQ2Ysd0JBQWMsRUFBRSxJQUFJO1NBQ3JCLENBQUM7QUFDRixZQUFJLGNBQWMsRUFBRTtBQUNsQixpQkFBTyxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUM7U0FDNUI7O0FBRUQsWUFBTSxNQUFNLEdBQUcsd0JBQ1EsZ0JBQWdCLEVBQ3JDLGVBQWUsRUFDZixBQUFDLE1BQU0sSUFBSSxJQUFJLGlCQUFnQixNQUFNLEdBQUssU0FBUyw0QkFDaEQsbUJBQW1CLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUNqQyxNQUFNLENBQUMsVUFBQSxDQUFDO2lCQUFJLENBQUM7U0FBQSxDQUFDLENBQUM7O0FBRWpCLFlBQU0sTUFBTSxHQUFHLE1BQU0sT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDOztBQUVuRSxZQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUUsS0FBSyxRQUFRLEVBQUU7O0FBRWpDLGlCQUFPLElBQUksQ0FBQztTQUNiOztBQUVELFlBQUksUUFBUSxZQUFBLENBQUM7QUFDYixZQUFJO0FBQ0Ysa0JBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQy9CLENBQUMsT0FBTyxLQUFLLEVBQUU7QUFDZCxjQUFNLE1BQU0sR0FBRyxxQ0FBcUMsQ0FBQztBQUNyRCxjQUFNLE1BQU0sR0FBRyw0Q0FBNEMsQ0FBQztBQUM1RCxjQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRTtBQUM5QyxnQkFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsMkNBQTJDLEVBQUU7QUFDdkUsb0JBQU0sRUFBRSxzRkFBc0Y7QUFDOUYseUJBQVcsRUFBRSxJQUFJO2FBQ2xCLENBQUMsQ0FBQzs7QUFFSCxtQkFBTyxJQUFJLENBQUM7V0FDYjs7QUFFRCxpQkFBTyxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUM7QUFDbkQsY0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLEVBQUU7QUFDaEQsa0JBQU0sRUFBRSwyRUFBMkU7QUFDbkYsdUJBQVcsRUFBRSxJQUFJO1dBQ2xCLENBQUMsQ0FBQzs7QUFFSCxpQkFBTyxJQUFJLENBQUM7U0FDYjs7QUFFRCxlQUFPLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxDQUFBLENBQUUsR0FBRyxDQUFDLFVBQUMsR0FBRyxFQUFLO0FBQ3JELGNBQUksS0FBSyxZQUFBLENBQUM7QUFDVixjQUFJLEdBQUcsQ0FBQyxNQUFNLEVBQUU7QUFDZCxpQkFBSyx1REFBbUQsR0FBRyxDQUFDLE1BQU0sYUFBVSxDQUFDO1dBQzlFOzs7QUFHRCxjQUFNLElBQUksR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFBLEdBQUksQ0FBQyxDQUFDO0FBQ2pDLGNBQU0sR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUEsR0FBSSxDQUFDLENBQUM7QUFDbEMsY0FBSSxLQUFLLFlBQUEsQ0FBQzs7QUFFVixjQUFJLEdBQUcsQ0FBQyxNQUFNLEVBQUU7O0FBRWQsaUJBQUssR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxFQUFFLEdBQUcsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztXQUNqRCxNQUFNOzs7QUFHTCxtQkFBTyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLE1BQU0sR0FBRyxDQUFDLEdBQUcsU0FBUyxDQUFDLENBQUM7V0FDOUU7O0FBRUQsaUJBQU87QUFDTCxnQkFBSSxFQUFFLEdBQUcsQ0FBQyxRQUFRLElBQUksT0FBTztBQUM3QixnQkFBSSxRQUFLLEtBQUssSUFBSSxFQUFFLENBQUEsSUFBRyxHQUFHLENBQUMsTUFBTSxJQUFJLGVBQWUsQ0FBQSxBQUFFO0FBQ3RELG9CQUFRLEVBQVIsUUFBUTtBQUNSLGlCQUFLLEVBQUwsS0FBSztXQUNOLENBQUM7U0FDSCxDQUFDLENBQUM7T0FDSixDQUFBO0tBQ0YsQ0FBQztHQUNIO0NBQ0YiLCJmaWxlIjoiL1VzZXJzL293YWluLy5hdG9tL3BhY2thZ2VzL2xpbnRlci1zY3NzLWxpbnQvbGliL2luaXQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIGJhYmVsJztcblxuLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIGltcG9ydC9uby1leHRyYW5lb3VzLWRlcGVuZGVuY2llcywgaW1wb3J0L2V4dGVuc2lvbnNcbmltcG9ydCB7IENvbXBvc2l0ZURpc3Bvc2FibGUgfSBmcm9tICdhdG9tJztcbmltcG9ydCAqIGFzIGhlbHBlcnMgZnJvbSAnYXRvbS1saW50ZXInO1xuaW1wb3J0IHsgZGlybmFtZSwgcmVsYXRpdmUgfSBmcm9tICdwYXRoJztcblxuLy8gU2V0dGluZ3NcbmxldCBleGVjdXRhYmxlUGF0aDtcbmxldCBhZGRpdGlvbmFsQXJndW1lbnRzO1xubGV0IGRpc2FibGVPbk5vQ29uZmlnO1xubGV0IGNvbmZpZ05hbWU7XG5sZXQgZGlzYWJsZVRpbWVvdXQ7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgYWN0aXZhdGUoKSB7XG4gICAgcmVxdWlyZSgnYXRvbS1wYWNrYWdlLWRlcHMnKS5pbnN0YWxsKCdsaW50ZXItc2Nzcy1saW50Jyk7XG5cbiAgICB0aGlzLnN1YnMgPSBuZXcgQ29tcG9zaXRlRGlzcG9zYWJsZSgpO1xuICAgIHRoaXMuc3Vicy5hZGQoXG4gICAgICBhdG9tLmNvbmZpZy5vYnNlcnZlKCdsaW50ZXItc2Nzcy1saW50LmV4ZWN1dGFibGVQYXRoJywgKHZhbHVlKSA9PiB7XG4gICAgICAgIGV4ZWN1dGFibGVQYXRoID0gdmFsdWU7XG4gICAgICB9LFxuICAgICksXG4gICAgKTtcbiAgICB0aGlzLnN1YnMuYWRkKFxuICAgICAgYXRvbS5jb25maWcub2JzZXJ2ZSgnbGludGVyLXNjc3MtbGludC5hZGRpdGlvbmFsQXJndW1lbnRzJywgKHZhbHVlKSA9PiB7XG4gICAgICAgIGFkZGl0aW9uYWxBcmd1bWVudHMgPSB2YWx1ZTtcbiAgICAgIH0pLFxuICAgICk7XG4gICAgdGhpcy5zdWJzLmFkZChcbiAgICAgIGF0b20uY29uZmlnLm9ic2VydmUoJ2xpbnRlci1zY3NzLWxpbnQuZGlzYWJsZVdoZW5Ob0NvbmZpZ0ZpbGVJblBhdGgnLCAodmFsdWUpID0+IHtcbiAgICAgICAgZGlzYWJsZU9uTm9Db25maWcgPSB2YWx1ZTtcbiAgICAgIH0pLFxuICAgICk7XG4gICAgdGhpcy5zdWJzLmFkZChcbiAgICAgIGF0b20uY29uZmlnLm9ic2VydmUoJ2xpbnRlci1zY3NzLWxpbnQuY29uZmlnTmFtZScsICh2YWx1ZSkgPT4ge1xuICAgICAgICBjb25maWdOYW1lID0gdmFsdWU7XG4gICAgICB9KSxcbiAgICApO1xuICAgIHRoaXMuc3Vicy5hZGQoXG4gICAgICBhdG9tLmNvbmZpZy5vYnNlcnZlKCdsaW50ZXItc2Nzcy1saW50LmRpc2FibGVUaW1lb3V0JywgKHZhbHVlKSA9PiB7XG4gICAgICAgIGRpc2FibGVUaW1lb3V0ID0gdmFsdWU7XG4gICAgICB9KSxcbiAgICApO1xuICB9LFxuXG4gIGRlYWN0aXZhdGUoKSB7XG4gICAgdGhpcy5zdWJzLmRpc3Bvc2UoKTtcbiAgfSxcblxuICBnZXRSZWxhdGl2ZUZpbGVQYXRoKGZpbGVQYXRoLCBjb25maWdQYXRoKSB7XG4gICAgaWYgKGNvbmZpZ1BhdGgpIHtcbiAgICAgIHJldHVybiByZWxhdGl2ZShkaXJuYW1lKGNvbmZpZ1BhdGgpLCBmaWxlUGF0aCk7XG4gICAgfVxuICAgIHJldHVybiBmaWxlUGF0aDtcbiAgfSxcblxuICBwcm92aWRlTGludGVyKCkge1xuICAgIHJldHVybiB7XG4gICAgICBuYW1lOiAnc2Nzcy1saW50JyxcbiAgICAgIGdyYW1tYXJTY29wZXM6IFsnc291cmNlLmNzcy5zY3NzJywgJ3NvdXJjZS5zY3NzJ10sXG4gICAgICBzY29wZTogJ2ZpbGUnLFxuICAgICAgbGludE9uRmx5OiB0cnVlLFxuICAgICAgbGludDogYXN5bmMgKGVkaXRvcikgPT4ge1xuICAgICAgICBjb25zdCBmaWxlUGF0aCA9IGVkaXRvci5nZXRQYXRoKCk7XG4gICAgICAgIGNvbnN0IGZpbGVUZXh0ID0gZWRpdG9yLmdldFRleHQoKTtcblxuICAgICAgICBpZiAoZmlsZVRleHQubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgcmV0dXJuIFtdO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgY29uZmlnID0gYXdhaXQgaGVscGVycy5maW5kQXN5bmMoZmlsZVBhdGgsIGNvbmZpZ05hbWUpO1xuICAgICAgICBjb25zdCByZWxhdGl2ZUZpbGVQYXRoID0gdGhpcy5nZXRSZWxhdGl2ZUZpbGVQYXRoKGZpbGVQYXRoLCBjb25maWcpO1xuXG4gICAgICAgIGlmIChkaXNhYmxlT25Ob0NvbmZpZyAmJiAhY29uZmlnKSB7XG4gICAgICAgICAgcmV0dXJuIFtdO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3Qgb3B0aW9ucyA9IHtcbiAgICAgICAgICBjd2Q6IGRpcm5hbWUoZmlsZVBhdGgpLFxuICAgICAgICAgIHN0ZGluOiBmaWxlVGV4dCxcbiAgICAgICAgICBpZ25vcmVFeGl0Q29kZTogdHJ1ZSxcbiAgICAgICAgfTtcbiAgICAgICAgaWYgKGRpc2FibGVUaW1lb3V0KSB7XG4gICAgICAgICAgb3B0aW9ucy50aW1lb3V0ID0gSW5maW5pdHk7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBwYXJhbXMgPSBbXG4gICAgICAgICAgYC0tc3RkaW4tZmlsZS1wYXRoPSR7cmVsYXRpdmVGaWxlUGF0aH1gLFxuICAgICAgICAgICctLWZvcm1hdD1KU09OJyxcbiAgICAgICAgICAoY29uZmlnICE9IG51bGwpID8gYC0tY29uZmlnPSR7Y29uZmlnfWAgOiB1bmRlZmluZWQsXG4gICAgICAgICAgLi4uYWRkaXRpb25hbEFyZ3VtZW50cy5zcGxpdCgnICcpLFxuICAgICAgICBdLmZpbHRlcihlID0+IGUpO1xuXG4gICAgICAgIGNvbnN0IG91dHB1dCA9IGF3YWl0IGhlbHBlcnMuZXhlYyhleGVjdXRhYmxlUGF0aCwgcGFyYW1zLCBvcHRpb25zKTtcblxuICAgICAgICBpZiAoZWRpdG9yLmdldFRleHQoKSAhPT0gZmlsZVRleHQpIHtcbiAgICAgICAgICAvLyBFZGl0b3IgY29udGVudHMgaGF2ZSBjaGFuZ2VkLCBkb24ndCB1cGRhdGUgbWVzc2FnZXNcbiAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBjb250ZW50cztcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICBjb250ZW50cyA9IEpTT04ucGFyc2Uob3V0cHV0KTtcbiAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgICBjb25zdCByZWdleDEgPSAvXmludmFsaWQgb3B0aW9uOiAtLXN0ZGluLWZpbGUtcGF0aD0vOyAgLy8gPDAuNDMuMFxuICAgICAgICAgIGNvbnN0IHJlZ2V4MiA9IC9edW5kZWZpbmVkIGxvY2FsIHZhcmlhYmxlIG9yIG1ldGhvZCBgZmlsZScvOyAgLy8gMC40My4wLCAwLjQzLjFcbiAgICAgICAgICBpZiAocmVnZXgxLmV4ZWMob3V0cHV0KSB8fCByZWdleDIuZXhlYyhvdXRwdXQpKSB7XG4gICAgICAgICAgICBhdG9tLm5vdGlmaWNhdGlvbnMuYWRkRXJyb3IoJ1lvdSBhcmUgdXNpbmcgYW4gb2xkIHZlcnNpb24gb2Ygc2Nzcy1saW50Jywge1xuICAgICAgICAgICAgICBkZXRhaWw6ICdQbGVhc2UgdXBncmFkZSB5b3VyIHZlcnNpb24gb2Ygc2Nzcy1saW50LlxcbkNoZWNrIHRoZSBSRUFETUUgZm9yIGZ1cnRoZXIgaW5mb3JtYXRpb24uJyxcbiAgICAgICAgICAgICAgZGlzbWlzc2FibGU6IHRydWUsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIC8vIFRlbGwgTGludGVyIG5vdCB0byB1cGRhdGUgdGhlIGN1cnJlbnQgbWVzc2FnZXMgKGlmIGFueSlcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgIH1cbiAgICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tY29uc29sZVxuICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ1tMaW50ZXItU0NTUy1MaW50XScsIGVycm9yLCBvdXRwdXQpO1xuICAgICAgICAgIGF0b20ubm90aWZpY2F0aW9ucy5hZGRFcnJvcignW0xpbnRlci1TQ1NTLUxpbnRdJywge1xuICAgICAgICAgICAgZGV0YWlsOiAnU0NTUy1MaW50IHJldHVybmVkIGFuIGludmFsaWQgcmVzcG9uc2UsIGNoZWNrIHlvdXIgY29uc29sZSBmb3IgbW9yZSBpbmZvLicsXG4gICAgICAgICAgICBkaXNtaXNzYWJsZTogdHJ1ZSxcbiAgICAgICAgICB9KTtcbiAgICAgICAgICAvLyBUZWxsIExpbnRlciBub3QgdG8gdXBkYXRlIHRoZSBjdXJyZW50IG1lc3NhZ2VzIChpZiBhbnkpXG4gICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gKGNvbnRlbnRzW3JlbGF0aXZlRmlsZVBhdGhdIHx8IFtdKS5tYXAoKG1zZykgPT4ge1xuICAgICAgICAgIGxldCBiYWRnZTtcbiAgICAgICAgICBpZiAobXNnLmxpbnRlcikge1xuICAgICAgICAgICAgYmFkZ2UgPSBgPHNwYW4gY2xhc3M9J2JhZGdlIGJhZGdlLWZsZXhpYmxlIHNjc3MtbGludCc+JHttc2cubGludGVyfTwvc3Bhbj4gYDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAvLyBBdG9tIGV4cGVjdHMgcmFuZ2VzIHRvIGJlIDAtYmFzZWRcbiAgICAgICAgICBjb25zdCBsaW5lID0gKG1zZy5saW5lIHx8IDEpIC0gMTtcbiAgICAgICAgICBjb25zdCBjb2wgPSAobXNnLmNvbHVtbiB8fCAxKSAtIDE7XG4gICAgICAgICAgbGV0IHJhbmdlO1xuXG4gICAgICAgICAgaWYgKG1zZy5sZW5ndGgpIHtcbiAgICAgICAgICAgIC8vIElmIHRoZSBtZXNzYWdlIGRlZmluZXMgYSBsZW5ndGgsIHVzZSB0aGF0XG4gICAgICAgICAgICByYW5nZSA9IFtbbGluZSwgY29sXSwgW2xpbmUsIGNvbCArIG1zZy5sZW5ndGhdXTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgLy8gT3RoZXJ3aXNlIGdlbmVyYXRlIGEgcmFuZ2UgZm9yIHRoZSBuZXh0IFwid29yZFwiIGFzIGRlZmluZWQgYnkgdGhlIGxhbmd1YWdlXG4gICAgICAgICAgICAvLyBOT1RFOiBDdXJyZW50IHZlcnNpb25zIG9mIHNjc3MtbGludCBzaG91bGQgX25ldmVyXyBoaXQgdGhpc1xuICAgICAgICAgICAgaGVscGVycy5nZW5lcmF0ZVJhbmdlKGVkaXRvciwgbGluZSwgbXNnLmNvbHVtbiA/IG1zZy5jb2x1bW4gLSAxIDogdW5kZWZpbmVkKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgdHlwZTogbXNnLnNldmVyaXR5IHx8ICdlcnJvcicsXG4gICAgICAgICAgICBodG1sOiBgJHtiYWRnZSB8fCAnJ30ke21zZy5yZWFzb24gfHwgJ1Vua25vd24gRXJyb3InfWAsXG4gICAgICAgICAgICBmaWxlUGF0aCxcbiAgICAgICAgICAgIHJhbmdlLFxuICAgICAgICAgIH07XG4gICAgICAgIH0pO1xuICAgICAgfSxcbiAgICB9O1xuICB9LFxufTtcbiJdfQ==