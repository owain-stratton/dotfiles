set nocompatible     "Use Vim settings, rather then Vi settings

execute pathogen#infect()
filetype plugin indent on

" ================ General Config ====================
set number           "Line numbers are useful
set visualbell       "No sounds

"Turn on syntax highlighting
syntax enable

set wrap             "Wrap lines
set linebreak        "Wrap lines at convenient points

set background=dark
"colorscheme material-theme

set tabstop=4        "Number of visual spaces per TAB
set softtabstop=4    "Number of spaces in tab when editing
set expandtab        "Tabs are spaces

filetype indent on   "Load filetype-specific indent files

" CtrlP settings
let g:ctrlp_match_window = 'bottom,order:ttb'
let g:ctrlp_switch_buffer = 0
let g:ctrlp_working_path_mode = 0
let g:ctrlp_user_command = 'ag %s -l --nocolor --hidden -g ""'

" Nerd Tree
" autocmd vimenter * NERDTree
map <C-n> :NERDTreeToggle<CR>

" Markdown
let g:vim_markdown_folding_disabled = 1

" Markdown Preview
let vim_markdown_preview_github=1
let vim_markdown_preview_browser='Google Chrome'
let vim_markdown_preview_hotkey='<C-m>'
